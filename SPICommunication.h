/******************************
*SPICommunications.h          *
*Helper functions to send     *
*data out the SPI port        *
*******************************/

#ifndef SPICOMMUNICATION_HEADER
#define SPICOMMUNICATION_HEADER

void uDelay(unsigned char delay);
void SPI_Init( void );
void ReadyPortForSPIWrite (unsigned char port);
void ReadyPortForSPIRead(unsigned char port);
void DisableSPI_ReadWrite(void);
void GenReadStrobe( void );
void pollSPI_1_Read (unsigned char SpiLastTxmt);
void pollSPI_1_Write(unsigned char outputByte);
void pollSPI_1_TransmitComplete(void);
void SPI_0_EnableReceiveInterrupt(void);
void SPI_1_EnableTransmitInterrupt(void);
void SPI_1_EnableReceiveInterrupt(void);
void SPI_1_DisableTransmitInterrupt(void);
void SPI_1_DisableReceiveInterrupt(void);
#endif

