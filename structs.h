//
// Author: Tom
//

#ifndef STRUCTS_H
#define STRUCTS_H


#include "TrakConstants.h"
#include "XLConstants.h"


struct Packet
{
  unsigned char status;
  unsigned char activeIndex;
  unsigned char data [PKT_MAX_LENGTH];
};


// XL

struct MiniPacket
{
  unsigned char status;
  unsigned char activeIndex;
  unsigned char data[MINI_PKT_MAX_LENGTH];
};



struct XLportX
{          
  // LightData contains the RGB data for the stick lights, formatted for the SPI port
  // BlinkData contains the blink attribute for each bit of LightData. Set the attribute bit to blink LED
  //                               0   1   2   3   4   5   6   7 
  unsigned char LightData[(MAX_XL_STICKS_PER_PORT*4) + (MAX_XL_STICKS_PER_PORT/2) +1];  // assumes 12" sticks
  unsigned char BlinkData[(MAX_XL_STICKS_PER_PORT*4) + (MAX_XL_STICKS_PER_PORT/2) +1];  // 9" sticks will use a subset of these arrays
};

struct PBSwitchX
{
  unsigned char Value;    // good for 4 buttons 
  unsigned char Status;
};

struct ButtonStix
{
  unsigned char Value;    // good for 4 buttons 
  unsigned char Status;
};

struct ButtonPortX
{
  struct ButtonStix ButtonStick[MAX_XL_BUTTONLIGHTS_PER_PORT]; 
};

struct ConfigPortx
{
  unsigned char deviceType[MAX_XL_BUTTONLIGHTS_PER_PORT];
  unsigned char deviceCount;
};

struct XLPickData
{
  unsigned int  seqNum;
  unsigned char color;
  unsigned char status;
  unsigned char begIdx;
  unsigned char endIdx;
  unsigned char begDevice;
  unsigned char begSWvalue;
  unsigned char endDevice;
  unsigned char endSWvalue;
  unsigned int  zone;
  unsigned int  section;
};

struct BayDispDevice
{
  unsigned long address;
  unsigned int timer;
};

// input portion
// BIT  Meaning
//  0   logic x     3 bit XL device ID pattern 
//  1   logic x
//  2   logic x
//  3   button8     located next to upstream connector
//  4   button4
//  5   button2
//  6   button1     located next to downstream connector (controller side)
//  7   logic 1     1 bit stick present pattern

// structure defining how buttons are read
struct inputButtons {
	unsigned int XLdeviceID     : 3;        // 3 bit XL device ID
    unsigned int button8    	: 1;        // button S4 (closest to controller) downstream end
	unsigned int button4		: 1;        // button S3
	unsigned int button2		: 1;        // button S2
	unsigned int button1		: 1;        // button S1 (upstream end of stick)		
	unsigned int stickPresent1  : 1;        // 1 bit stick present pattern. logic 1
};
struct twoNibbles {
	unsigned char n1			: 4;        // these are nibbles so don't need to be swapped
	unsigned char n2			: 4;
};
union xlButtons {
	struct inputButtons b;
	struct twoNibbles	n;
	unsigned char		c;
};

										
#endif
