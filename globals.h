#ifndef GLOBALS_H     
#define GLOBALS_H

//
// Author: Lamar
//
//


#include "structs.h"
#include "TrakConstants.h"


void InitializeGlobals (void);
void SetState (enum TrakStates state);


// page zero variables

extern unsigned char bootStatus;



// in page zero


 // XL Controller
extern volatile struct XLportX XLport[];
extern volatile struct ButtonPortX ButtonPort[];
extern volatile struct ConfigPortx XLConfigPort[];
extern volatile struct MiniPacket rxSPIpacket[];
extern volatile unsigned char SPItoMaster[];
extern volatile unsigned char activeSPIBufIdx;
extern volatile unsigned char processSPIBufIdx;
extern volatile unsigned rti_ctr;
extern volatile unsigned last_rti_cnt;
extern volatile unsigned char diagnosticMode;
extern volatile unsigned char downloadDisplayMode;
extern volatile unsigned char SPI_0_outputPtr;
extern volatile unsigned char SPI_1_outputPtr;
extern volatile unsigned char portToUpload;
extern volatile unsigned char XLportBlinkControl;
extern volatile unsigned char XLportToUpdate;

extern unsigned char checkSumBootStrap;
extern unsigned char checkSumMainCode;


#define  SET_BIT(var,mask)          (var |= mask)
#define  CLR_BIT(var,mask)          (var &= ~(mask))


#endif // ifndef GLOBALS_H
