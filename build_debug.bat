@echo off
ca6808 -v "crtsi_NO_WDT.s"
if errorlevel 1 goto bad
cx6808 %1 -v -l -dxdebug +debug -pp -pck -e -no diagState.c
if errorlevel 1 goto bad
cx6808 %1 -v -l -dxdebug +debug -pp -pck -e -no displayDriver.c
if errorlevel 1 goto bad
cx6808 %1 -v -l -dxdebug +debug -pp -pck -e -no globals.c
if errorlevel 1 goto bad
cx6808 %1 -v -l -dxdebug +debug -pp -pck -e -no interrupts.c
if errorlevel 1 goto bad
cx6808 %1 -v -l -dxdebug +debug -pp -pck -e -no interruptSPI_0.c
if errorlevel 1 goto bad
cx6808 %1 -v -l -dxdebug +debug -pp -pck -e -no interruptSPI_1.c
if errorlevel 1 goto bad
cx6808 %1 -v -l -dxdebug +debug -pp -pck -e -no main.c
if errorlevel 1 goto bad
cx6808 %1 -v -l -dxdebug +debug -pp -pck -e -no SPICommunication.c
if errorlevel 1 goto bad
cx6808 %1 -v -l -dxdebug +debug -pp -pck -e -no vectors_pt60.c
if errorlevel 1 goto bad


:clink
echo.
echo Linking ...
clnk -sa -p -oXL9-PT32.S08 -mXL9-PT32.map XL9-PT32.lkf
if errorlevel 1 goto bad

:chexa
echo.
echo Converting ...
chex +hXL -fm -oXL9-PT32.s19 XL9-PT32.s08
if errorlevel 1 goto bad

:cllabs
echo.
echo Generating absolute listing ...
clabs XL9-PT32.s08
if errorlevel 1 goto bad
echo.
echo.
echo        The Compilation is successfull.
echo.
rem pause
goto sortie
:bad
echo.
echo.
echo        THE COMPILATION FAILED.
echo.
pause
:sortie


del *.o

echo on

