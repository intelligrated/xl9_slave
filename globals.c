#include <limits.h>
#include <string.h>

#include "structs.h"
#include "trakConstants.h"


@tiny unsigned char bootStatus;

// page zero variables


//////// for XL Controller
volatile struct XLportX XLport[MAX_XL_PORTS];
volatile struct ButtonPortX ButtonPort[MAX_XL_PORTS];
volatile struct ConfigPortx XLConfigPort[MAX_XL_PORTS];
volatile struct MiniPacket rxSPIpacket[MAX_SPI_PACKETS];
volatile unsigned char SPItoMaster[MAX_XL_BUTTONLIGHTS_PER_PORT+5];
@tiny volatile unsigned char activeSPIBufIdx;
@tiny volatile unsigned char processSPIBufIdx;
@tiny volatile unsigned rti_ctr;
@tiny volatile unsigned last_rti_cnt;
@tiny unsigned char diagnosticMode;
@tiny unsigned char downloadDisplayMode;
@tiny volatile unsigned char SPI_0_outputPtr;
@tiny volatile unsigned char SPI_1_outputPtr;
@tiny volatile unsigned char portToUpload;
@tiny volatile unsigned char XLportToUpdate;
@tiny volatile unsigned char XLportBlinkControl;


@tiny unsigned char checkSumBootStrap;
@tiny unsigned char checkSumMainCode;

void
InitializeGlobals (void)
{
  unsigned int tmpUInt;

  rti_ctr = 0;
  activeSPIBufIdx = 0;
  processSPIBufIdx = 0;
  diagnosticMode = FALSE;
  downloadDisplayMode = FALSE;
  SPI_0_outputPtr = 0;
  SPI_1_outputPtr = 0;
  portToUpload = 0xff;
  memset(&rxSPIpacket, 0, sizeof(rxSPIpacket));
   
/*
  checkSumMainCode = 0;
  for (tmpUInt=0x1840; tmpUInt < 0xF000; tmpUInt++)
    checkSumMainCode ^= *((unsigned char*) (tmpUInt));
  checkSumBootStrap = 0;
  for (tmpUInt=0xF000; tmpUInt < 0xFFAC; tmpUInt++)
    checkSumBootStrap ^= *((unsigned char*) (tmpUInt));
*/

} // End of InitializeGlobals (void)
