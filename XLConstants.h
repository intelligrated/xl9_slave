//
// Author: tpr
//

#ifndef XLCONSTANTS_H
#define XLCONSTANTS_H


#define BAY_DISP_DEV_ACTIVITY_TC 120  // a bay disp device must respond every (120) secs or it may be replaced
#define PWR_ON_RESET    0x80          // POR status bit in SRS register
#define ILLEGAL_OPCODE_RESET 0x10     // ILOP bit in SRS register

#define MINI_PKT_MAX_LENGTH 8
#define MAX_SPI_PACKETS 20
// commands to slave processor
#define CMD_DISPLAY_CLEAR     0xc0
#define CMD_DIAGNOSTIC_STATE  0xc1
#define CMD_LED_RAM_UPDATE1   0xc2
#define CMD_LED_RAM_UPDATEx   0xc3
#define CMD_PORT_UPDATE       0xc4
#define CMD_PORT_CLEAR        0xc5
#define CMD_RUN_AUTOCONFIG    0xc6
#define CMD_INDICATE_DOWNLOAD 0xc7
#define CMD_UPDATE_ALL_PORTS  0xc8
#define CMD_SET_PORT_DEVICES  0xc9
#define CMD_COLOR_FILL_PORT   0xca
// commands initiated by slave
#define CMD_XLBUTTON          0xcc
#define CMD_PORT_DEVICES      0xcd


// handshake lines
#define SlaveDataReadyN  PH2
#define SendSlaveDataN   PE7
// SPI
#define SPI0CNTL1       SPI0_C1
#define SPI0CNTL2       SPI0_C2
#define SPI0BAUD        SPI0_BR
#define SPI0STATUS      SPI0_S
#define SPI0DATA        SPI0_D

                                              
#define SPI0TXEMPTY      (SPI0STATUS & 0x20)
#define SPI0RXFULL       (SPI0STATUS & 0x80)

#define SPICLKPRESCALE  4     // 4: divide by 5   
#define SPICLKDIV       3     // 5: div by 64, 4: div by 32, 3 => 16, 2 => 8  (5 * 64 = 320 [18.8MHz/320 => 58.75 Kbaud]
#define SPI1DIV         ((SPICLKPRESCALE<<4)|SPICLKDIV)  // clk rate division (5 * 32 = 160 -> 117 Kbaud)
                                                         // clk rate division (5 * 16 = 80 -> 235 Kbaud)
                                                         // clk rate division (5 * 8 = 40 -> 470 Kbaud) does NOT work for XL application
#define SPI1CNTL1       SPI1_C1
#define SPI1CNTL2       SPI1_C2
#define SPI1BAUD        SPI1_BR
#define SPI1STATUS      SPI1_S
#define SPI1DATA_H      SPI1_DH     // 16 bit mode
#define SPI1DATA_L      SPI1_DL
                                              
#define SPI1TXEMPTY      (SPI1STATUS & 0X20)
#define SPI1RXFULL       (SPI1STATUS & 0X80)
						  // ssn = gpio, lsbit 1st out
#define SPTIE			0x20  // Transmit Interrupt Enable bit
#define SPIE			0x80  // Receive Interrupt Enable bit
#define SPE             0x40  // SPI port enable

#define SPI0_CFG1       0x40  // spi enable, slave mode
#define SPI0_CFG2       0x00  // unused7, unused6, unused5, ssn = slave select (mandatory in slave mode), 
#define SPI1_CFG2       0x08  // disable match int, 8 bit mode, clk active in wait mode, 2 pin data xfer

#define SPI1_CFG1_READ    0x5B  // READ, difference from WRITE is spi clk idle hi
#define SPI1_CFG1_WRITE   0x53  // no ints, spi enab, no tx int, master, lsb 1st, clk phase @ bit mid, clk polarity idle lo


//#define SPICFG1         0x57  // no ints, spi enab, no tx int, master, 
                              // clk polarity idle lo, clk phase 1st edge @ start, ssn = gpio, lsbit 1st out

 
#define TURNOFFSPTIE   	0xDF  // Transmit Interrupt Enable bit


#endif 
