//
// Author: tom
// Thu Aug  5 12:25:15 PDT 2010
//

#include <string.h>

#include "diagState.h"
#include "structs.h"
#include "globals.h"
#include "TrakConstants.h"
#include "displayDriver.h"


unsigned char diagTimer;  // keeps lights from being "all on" forever

 
unsigned char                   // increments the color, which is in negative logic
IncBLColor(unsigned char color) //  (a logic 0 activates the led)
{                               // returns the incremented color in positive logic
  
  color = (unsigned char)(color ^ 0x07);// convert to active hi logic
  color &= 0x07;                        // limit range to 0 - 7
  color++;                              // inc to next color
  if (color == 0x08)                    // if we inc'ed white
    color = LT_BLUE;                    // start over with blue
  return(color);
  
}

void
ActivateDiagState (void) {
  
  unsigned char iport, device, j, deviceCount;

  ClearAllPorts();        // turn off all LEDs
  diagTimer = 0;
  
  for (iport = 0; iport < MAX_XL_PORTS; iport++)
  {
    deviceCount = XLConfigPort[iport].deviceCount;
    if (!deviceCount)
      continue;
    
    for (device = 0; device < deviceCount; device++) // for each stick on this port
    {
      if ((XLConfigPort[iport].deviceType[device] == XL_12STICK_W_BUTTONS) ||
          (XLConfigPort[iport].deviceType[device] == XL_12STICK_WO_BUTTONS))
      {
        UpdateLEDMemory(iport, (device*MAX_XL_RGBLEDS_PER_12STICK)+1, LT_RED, LT_SOLID); // led1 red
        UpdateLEDMemory(iport, (device*MAX_XL_RGBLEDS_PER_12STICK)+4, LT_LIME, LT_SOLID);   // led4 lime
        UpdateLEDMemory(iport, (device*MAX_XL_RGBLEDS_PER_12STICK)+7, LT_BLUE, LT_SOLID);  // led7 blue
        UpdateLEDMemory(iport, (device*MAX_XL_RGBLEDS_PER_12STICK)+10, LT_WHITE, LT_SOLID); // led10 white
      }
      else if ((XLConfigPort[iport].deviceType[device] == XL_9STICK_W_BUTTONS) ||
          (XLConfigPort[iport].deviceType[device] == XL_9STICK_WO_BUTTONS))
      {
        UpdateLEDMemory(iport, (device*MAX_XL_RGBLEDS_PER_9STICK)+1, LT_RED, LT_SOLID); // led1 red
        UpdateLEDMemory(iport, (device*MAX_XL_RGBLEDS_PER_9STICK)+4, LT_LIME, LT_SOLID);   // led4 lime
        UpdateLEDMemory(iport, (device*MAX_XL_RGBLEDS_PER_9STICK)+7, LT_BLUE, LT_SOLID);  // led7 blue
      }
      else  // for button lights
      {
        UpdateLEDMemory(iport, (device*2)+0, LT_BLUE, LT_SOLID); // 2 LEDs per button light
        UpdateLEDMemory(iport, (device*2)+1, LT_BLUE, LT_SOLID);
      }
    }
    SendPortLEDdata(iport, FALSE);  // update physical LEDs
  } // end of for(iport...
}// ActivateDiagState


void
DiagState (void)
{

  unsigned char iport, device, deviceCount, ledNum, fillColor;
  unsigned char BLcolor, i;
  static unsigned lastRTICnt;


  if (diagTimer)  // if timer active
  {
    if (lastRTICnt != rti_ctr) // rtc interrupts every 5 mS
    {
      lastRTICnt = rti_ctr;
      if (--diagTimer == 0)
      {
        ActivateDiagState();    // revert LEDs to initial state
        return;
      }
    }
  }


  for (iport = 0; iport < MAX_XL_PORTS; iport++)
  {
    deviceCount = XLConfigPort[iport].deviceCount;
          
    if (!deviceCount)   // if no devices on port, goto next port
      continue;

    for (device = 0; device < deviceCount; device++)
    {
      if (ButtonPort[iport].ButtonStick[device].Status == BUTTON_RELEASED)
      {
        if (XLConfigPort[iport].deviceType[device] == XL_BL_1_BUTTON)   // if 1 button Button Light
        {
          BLcolor = (unsigned char)(XLport[iport].LightData[device] & 0x07);     // get current color
          BLcolor = IncBLColor(BLcolor);                        // increment to next color
          UpdateLEDMemory(iport, (device*2)+0, BLcolor, LT_SOLID); // update memory image
          UpdateLEDMemory(iport, (device*2)+1, BLcolor, LT_SOLID);
        }
        else if (XLConfigPort[iport].deviceType[device] == XL_BL_2_BUTTON) // if 2 button Button Light
        {
          switch (ButtonPort[iport].ButtonStick[device].Value)
          {
            case S1_BUTTON:
              BLcolor = (unsigned char)(XLport[iport].LightData[device] & 0x38);   // get current color (2 msbits are tail lights)
              BLcolor = IncBLColor(BLcolor >> 3);               // inc to next color
              UpdateLEDMemory(iport, (device*2)+0, BLcolor, LT_SOLID); 
              break;

            case S2_BUTTON:
              BLcolor = (unsigned char)(XLport[iport].LightData[device] & 0x07);   // get current color
              BLcolor = IncBLColor(BLcolor);                    // inc to next color
              UpdateLEDMemory(iport, (device*2)+1, BLcolor, LT_SOLID); 
              break;
          }
        }
        else    // else stick light 
        {
          switch (ButtonPort[iport].ButtonStick[device].Value)
          {
            case S1_BUTTON:
              fillColor = LT_RED;
              break;

            case S2_BUTTON:
              fillColor = LT_LIME;
              break;

            case S3_BUTTON:
              fillColor = LT_BLUE;
              break;

            case S4_BUTTON:
              fillColor = LT_WHITE;
              break;

            default:
              fillColor = LT_CYAN;    // should never happen
          }
          if (XLConfigPort[iport].deviceType[device] == XL_12STICK_W_BUTTONS) 
          {
            //if (XLport[iport].LightData[device] == 0x3f) // if LEDs 8 & 9 OFF, color fill *** only fill blank displays????? ****
            {
              for (i = 0; i < deviceCount; i++)  // if there are any sticks w/o buttons on this port
              {                                  //  fill them too
                if ((XLConfigPort[iport].deviceType[i] == XL_12STICK_WO_BUTTONS) || (i == device)) 
                  for (ledNum = 0; ledNum < MAX_XL_RGBLEDS_PER_12STICK; ledNum++)
                    UpdateLEDMemory(iport, (i * MAX_XL_RGBLEDS_PER_12STICK)+ledNum, fillColor, LT_SOLID);
              }
            }
          }
          else    // else it's a 9" stick with buttons
          {
            //if (XLport[iport].LightData[device] == 0x3f) // if LEDs 8 & 9 OFF, color fill *** only fill blank displays????? ****
            {
              for (i = 0; i < deviceCount; i++)  // if there are any sticks w/o buttons on this port
              {                                  //  fill them too
                if ((XLConfigPort[iport].deviceType[i] == XL_9STICK_WO_BUTTONS) || (i == device)) 
                  for (ledNum = 0; ledNum < 9; ledNum++)
                    UpdateLEDMemory(iport, (i * MAX_XL_RGBLEDS_PER_9STICK)+ledNum, fillColor, LT_SOLID);
              }
            }
          }
        } // end of else STICK LIGHT 
        diagTimer = 200;   // 1000 mS / 5 mS   1 second timer constant
        ButtonPort[iport].ButtonStick[device].Value = 0;
        ButtonPort[iport].ButtonStick[device].Status = BUTTON_CLEAR;
        SendPortLEDdata(iport, FALSE);  // update physical LEDs on this port
      } // end of if (.status == RELEASED
    } // for (device)
  }  // for (iport)
}// DiagState
