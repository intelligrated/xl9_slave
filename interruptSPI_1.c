// interruptSPI_1.c


#include <limits.h>

#include "iospt60.h"
#include "globals.h"
#include "TrakConstants.h"
#include "XLconstants.h"
#include "SPICommunication.h"



@interrupt void
Vspi1 (void)
{
  unsigned char SPIoutputByte;
  
  if (SPI1TXEMPTY)   // read status register
  {
    if (SPI_1_outputPtr == 0)  // if last byte of buffer was sent LAST interrupt...
    {
      SPI_1_DisableTransmitInterrupt(); // disable the interrupt
      pollSPI_1_TransmitComplete();     // wait till last byte completely shifted out SPI port
      DisableSPI_ReadWrite(); // disable all SPI sub ports
      
    }
    else  // more data to send
    {
      SPI_1_outputPtr--;
      if (XLportBlinkControl == TRUE)
        SPIoutputByte = (unsigned char)(XLport[XLportToUpdate].LightData[SPI_1_outputPtr] | XLport[XLportToUpdate].BlinkData[SPI_1_outputPtr]);  // turn off lights with blink attribute set
      else
        SPIoutputByte = XLport[XLportToUpdate].LightData[SPI_1_outputPtr];
      
      SPI1DATA_L = SPIoutputByte;    // send LED data via the SPI port
    }                             // writing to data register clears interrupt
  }
    
}
