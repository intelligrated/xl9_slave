#ifndef PORTBITS_H
#define PORTBITS_H

// make 68hcs08 PORTs bit addressable
#include "iospt60.h"


_Bool PA0    @PORT_PTAD:0;
_Bool PA1    @PORT_PTAD:1;
_Bool PA2    @PORT_PTAD:2;
_Bool PA3    @PORT_PTAD:3;
_Bool PA4    @PORT_PTAD:4;
_Bool PA5    @PORT_PTAD:5;
_Bool PA6    @PORT_PTAD:6;
_Bool PA7    @PORT_PTAD:7;

_Bool PB0    @PORT_PTBD:0;
_Bool PB1    @PORT_PTBD:1;
_Bool PB2    @PORT_PTBD:2;
_Bool PB3    @PORT_PTBD:3;
_Bool PB4    @PORT_PTBD:4;
_Bool PB5    @PORT_PTBD:5;
_Bool PB6    @PORT_PTBD:6;
_Bool PB7    @PORT_PTBD:7;

_Bool PC0    @PORT_PTCD:0;
_Bool PC1    @PORT_PTCD:1;
_Bool PC2    @PORT_PTCD:2;
_Bool PC3    @PORT_PTCD:3;
_Bool PC4    @PORT_PTCD:4;
_Bool PC5    @PORT_PTCD:5;
_Bool PC6    @PORT_PTCD:6;
_Bool PC7    @PORT_PTCD:7;

_Bool PD0    @PORT_PTDD:0;
_Bool PD1    @PORT_PTDD:1;
_Bool PD2    @PORT_PTDD:2;
_Bool PD3    @PORT_PTDD:3;
_Bool PD4    @PORT_PTDD:4;
_Bool PD5    @PORT_PTDD:5;
_Bool PD6    @PORT_PTDD:6;
_Bool PD7    @PORT_PTDD:7;

_Bool PE0    @PORT_PTED:0;
_Bool PE1    @PORT_PTED:1;
_Bool PE2    @PORT_PTED:2;
_Bool PE3    @PORT_PTED:3;
_Bool PE4    @PORT_PTED:4;
_Bool PE5    @PORT_PTED:5;
_Bool PE6    @PORT_PTED:6;
_Bool PE7    @PORT_PTED:7;

_Bool PF0    @PORT_PTFD:0;
_Bool PF1    @PORT_PTFD:1;
_Bool PF2    @PORT_PTFD:2;
_Bool PF3    @PORT_PTFD:3;
_Bool PF4    @PORT_PTFD:4;
_Bool PF5    @PORT_PTFD:5;
_Bool PF6    @PORT_PTFD:6;
_Bool PF7    @PORT_PTFD:7;

_Bool PG0    @PORT_PTGD:0;
_Bool PG1    @PORT_PTGD:1;
_Bool PG2    @PORT_PTGD:2;
_Bool PG3    @PORT_PTGD:3;
//_Bool PG4    @PORT_PTGD:4;
//_Bool PG5    @PORT_PTGD:5;
//_Bool PG6    @PORT_PTGD:6;
//_Bool PG7    @PORT_PTGD:7;

_Bool PH0    @PORT_PTHD:0;
_Bool PH1    @PORT_PTHD:1;
_Bool PH2    @PORT_PTHD:2;
_Bool PH6    @PORT_PTHD:6;
_Bool PH7    @PORT_PTHD:7;


#endif // PORTBITS_H
