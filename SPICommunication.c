// filename: SPICommunication.c
//

#include "SPICommunication.h"
#include "TrakConstants.h"
#include "portBits.h"
#include "XLConstants.h"

//unsigned char SpiTransmitBuffer[MAX_XL_STICKS_PER_PORT * 5];
unsigned char SPIbuffer[MAX_XL_BUTTONLIGHTS_PER_PORT * MAX_XL_READ_BYTES_PER_STICK];
// NOTE: Data sheet was wrong! 55 is the address of the SPI Data Port (AC16)


void uDelay(unsigned char u)    // software time delay
{                               // NOTE: lower numbers are less accurate
  while(u--);                   // 2:2us,  100:100us,  (these require int argument:: 1000: 1.2ms,  10000: 10ms)
} // end uDelay()


void
SPI_Init( void )    // initialize the SPI ports
{

  // init SPI port 0 - slave mode, communicates with the master CPU
  SPI0CNTL1 = SPI0_CFG1;      // slave mode
  SPI0CNTL2 = SPI0_CFG2;      //                               
                              // slave mode, so no need to specify baud rate
  // init SPI port 1 - master mode, controls lights and PB switches
  SPI1CNTL1 = SPI1_CFG1_READ; // master mode
  SPI1CNTL2 = SPI1_CFG2;
  SPI1BAUD =  SPI1DIV;
}

void
ReadyPortForSPIWrite (unsigned char port) // config SPI port 1 & enable appropriate buffers
{
  SPI1CNTL1 = SPI1_CFG1_WRITE;  // prepare SPI port to WRITE

  switch (port)
  {
	case 0:
	  PC0 = 0;
	  break;
	case 1:                     
	  PC1 = 0;
	  break;
	case 2:
	  PC2 = 0;
	  break;
	case 3:
	  PC3 = 0;
	  break;
	case 4:
	  PB6 = 0;
	  break;
	case 5:
	  PB7 = 0;
	  break;
	case 6:
	  PC6 = 0;
	  break;
	case 7:
	  PC7 = 0;
	  break;
	case 8:
	  PE0 = 0;
	  break;
	case 9:
	  PE1 = 0;
	  break;

   } // end switch    
}

void
ReadyPortForSPIRead(unsigned char port)  // enable tri-state buffers for SPI input port
{
  
  SPI1CNTL1 = SPI1_CFG1_READ;     // prepare SPI port to read

  switch (port)            // now enable appropriate buffers
  {
  case 0: 
	PA0 = 0;
	break;
  case 1: 
	PA1 = 0;
	break;
  case 2: 
	PA6 = 0;
	break;
  case 3: 
	PA7 = 0;
	break;
  case 4: 
	PD4 = 0;
	break;
  case 5: 
	PD5 = 0;
	break;
  case 6: 
	PD6 = 0;
	break;
  case 7: 
	PD7 = 0;
	break;
  case 8: 
	PB0 = 0;
	break;
  case 9: 
	PB1 = 0;
	break;

  }

} // End ReadyPortForSPIRead


void
DisableSPI_ReadWrite(void)  // tri-state all SPI port buffers
{
  
  PORT_PTAD = 0xff;
  PORT_PTBD = 0xff;
  PORT_PTCD = 0xff;
  PD4 = 1;      // don't disturb PD0 - PD3 incase SPI1 is disabled
  PD5 = 1;
  PD6 = 1;
  PD7 = 1;
  PORT_PTED = 0xff;
  
} // End DisableReadWrite

void
GenReadStrobe( void ) // uses the down stream data line to 
{                     // strobe button data into shift regs (all ports)

  PD1 = 1;      // "MOSI" = 1
  PD0 = 0;      // "SPICLK" = 0 (idle state during SPI writes)

  SPI1CNTL1 &= (unsigned char) ~SPE;    // turn off SPI port (Port D bits 0 - 3 now parallel IO)

  
  PORT_PTCD = 00;    //  enable DS tri-state drivers for ports 0,1,2,3,6,7 (CSNOUTxx)
  PB6 = 0;      //                "                 port 4
  PB7 = 0;      //                "                 port 5
  PE0 = 0;      //                                  port 8
  PE1 = 0;      //                                  port 9

  PD1 = 0;      // generate load strobe for button shift registers, all 10 ports
				// autoconfig did not work with 5uS pulse + 25 ft cable
				// autoconfig worked with 10 uS + 25 ft cable, button reading spotty
  //uDelay(5);   // make strobe 5 uS wide -> too short with a 25 ft cable, 10 uS too
  uDelay(20);   // 15 uS did not work, 16 uS did; using 20 uS to have some margin

  PD1 = 1;

  DisableSPI_ReadWrite(); // disable DS tri-state drivers

  SPI1CNTL1 |= SPE;     // enable SPI port 1
}
 

void
pollSPI_1_Read (unsigned char SpiLastTxmt)
{
  // set SpiLastTxmt to the # of bytes to read from SPI port
  // this version optimized for minimum time between SPI port write/reads (7 uS)

  unsigned char dummy;
  unsigned char MoreDataToSend = TRUE;
  unsigned char SpiNextTxmt;
  unsigned char SpiNextRcv;
  int counter;

		
  SpiNextRcv = 0;
  SpiNextTxmt = 0;
  
  while (SPI1RXFULL)  // make sure no data in the receive buffer
  {
	dummy = SPI1DATA_L;
  }
 

  while (MoreDataToSend) // if we have something to transmit
  {
	// assume the Transmit Buffer is empty
	SPI1DATA_L = 0xff; // load dummy byte to SPI data buffer
    SpiNextTxmt++; // (you have to transmit a byte to receive a byte)

	// while waiting for shifting to complete, do some overhead chores
	if (SpiNextRcv >= MAX_XL_BUTTONLIGHTS_PER_PORT * MAX_XL_READ_BYTES_PER_STICK) // don't write past the end of the buffer
	  SpiNextRcv = (MAX_XL_BUTTONLIGHTS_PER_PORT * MAX_XL_READ_BYTES_PER_STICK) - 1;
	if (SpiNextTxmt >= SpiLastTxmt)
	  MoreDataToSend = FALSE;
    
	while(!SPI1RXFULL)
	  ;
    
	SPIbuffer[SpiNextRcv++] = SPI1DATA_L;   // this also clears SPIRXFULL
    
  } // while
}     // end of pollSPI_ReadOnly()


void
pollSPI_1_Write(unsigned char outputByte)
{
  while (!SPI1TXEMPTY)     // spin till SPI tx data bfr empty
    ;
  
  SPI1DATA_L = outputByte; 

}

void
pollSPI_1_TransmitComplete(void)
{
    unsigned int failsafe;
    unsigned char dummy;

	while (!SPI1TXEMPTY);  // wait for TX buffer to empty
						  // then we must wait for the transmit shift register to empty
	while (SPI1RXFULL)     // clear the SPI receive buffer full flag
	{                     // (SPI transmit and receive functions happen simultaneously)
	  dummy = SPI1DATA_L;
	}

	failsafe = 500;
	while (failsafe)
	{
	  failsafe--;
	  if (SPI1RXFULL)    // RX buffer full means transmit function is complete
		break;
	}
}

void
SPI_0_EnableReceiveInterrupt (void)
{

  SPI0CNTL1 |= SPIE;   // enable receive buffer full interrupt
}


void
SPI_1_EnableTransmitInterrupt (void)
{

  SPI1CNTL1 |= SPTIE;   // enable transmit buffer empty interrupt
}

void
SPI_1_EnableReceiveInterrupt (void)
{

  SPI1CNTL1 |= SPIE;   // enable receive buffer full interrupt
}


void
SPI_1_DisableTransmitInterrupt (void)
{

  SPI1CNTL1 &= (unsigned char)(~SPTIE);   // disable transmit buffer empty interrupt
}

void
SPI_1_DisableReceiveInterrupt (void)
{

  SPI1CNTL1 &= (unsigned char)(~SPIE);   // disable receive buffer full interrupt
}


