// interruptSPI_0.c
//
// Author: Tom
//

#include <limits.h>
#include <string.h>
#include <stdlib.h>


//#include "XLConstants.h"
#include "globals.h"
#include "portbits.h"


@interrupt void
Vspi0()
{
  unsigned char readData;
  struct MiniPacket *bufptr;
  unsigned char nextBufferIdx;


  if (SPI0RXFULL)   // read status register, check for rx buff full
  {
    readData = SPI0DATA;    // read data & clear interrupt
    if (SendSlaveDataN == 0)
    {
      SPI0DATA = SPItoMaster[SPI_0_outputPtr++];
      return;
    }
    bufptr = &rxSPIpacket[activeSPIBufIdx];

    if (readData == PKT_START_BYTE)
    {
      bufptr->activeIndex = 0;
      bufptr->status = PKT_RX_RX_RECEIVING;
      return;
    }
    if (readData == PKT_STOP_BYTE)
    {
      // verify packet.
      if (bufptr->activeIndex >= MINI_PKT_MAX_LENGTH)
        goto reset;
      
      // Is packet long enough?
      if (bufptr->activeIndex == 0) // command with no data
        goto reset;
      
      nextBufferIdx = (unsigned char)(activeSPIBufIdx+1);
      if (nextBufferIdx >= MAX_SPI_PACKETS)
        nextBufferIdx = 0;
      
      if (rxSPIpacket[nextBufferIdx].status != PKT_RX_EMPTY)
        goto reset;     // queue is full, abort
      
      bufptr->status = PKT_RX_READ;
      activeSPIBufIdx = nextBufferIdx;
      bufptr = &rxSPIpacket[activeSPIBufIdx];  // point to next buffer to fill
      goto reset;
    } // end of if (readData = PKT_STOP_BYTE)
  
    if (bufptr->activeIndex >= MINI_PKT_MAX_LENGTH)
      goto reset;
  
    if (bufptr->activeIndex == 0 && bufptr->status == PKT_RX_EMPTY) 
      goto reset; // wait for start byte
    
    bufptr->data [bufptr->activeIndex] = readData;
    bufptr->activeIndex++;
    return;
  } // end of if (SPI0RXFULL)
  // note the transmit interrupt is never enabled
  return;

reset:
  bufptr->activeIndex = 0;
  bufptr->status = PKT_RX_EMPTY;

} // end of interruptVspi0


