/*	Vector table for MC9S08PT60, PT32
 *	
 */
#define NULL 0
extern void _stext(void); // startup routine
extern void Vnvm(void);
extern void Vkbi1(void);
extern void Vkbi0(void);
extern void Vtsi(void);
extern void Vrtc(void);
extern void Viic(void);
extern void Vspi1(void);
extern void Vspi0(void);
extern void Vsci2txd(void);
extern void Vsci2rxd(void);
extern void Vsci2err(void);
extern void Vsci1txd(void);
extern void Vsci1rxd(void);
extern void Vsci1err(void);
extern void Vsci0txd(void);
extern void Vsci0rxd(void);
extern void Vsci0err(void);
extern void Vadc(void);
extern void Vacmp(void);
extern void Vmtim1(void);
extern void Vmtim0(void);
extern void Vftm0ovf(void);
extern void Vftm0ch1(void);
extern void Vftm0ch0(void);
extern void Vftm1ovf(void);
extern void Vftm1ch1(void);
extern void Vftm1ch0(void);
extern void Vftm2ovf(void);
extern void Vftm2ch5(void);
extern void Vftm2ch4(void);
extern void Vftm2ch3(void);
extern void Vftm2ch2(void);
extern void Vftm2ch1(void);
extern void Vftm2ch0(void);
extern void Vftm2flt(void);
extern void Vclk(void);
extern void Vlvw(void);
extern void Virq(void);
extern void Vswi(void);


void (* const _vectab[])() = {
  Vnvm,     // @ffb0  NVM
  Vkbi1,    // @ffb2  KBI1
  Vkbi0,    // @ffb4  KBI0
  Vtsi,     // @ffb6  Touch Screen Interface
  Vrtc,     // @ffb8  RTC
  Viic,     // @ffba  IIC
  Vspi1,    // @ffbc  SPI1         
  Vspi0,    // @ffbe  SPI0         
  Vsci2txd, // @ffc0  SCI2 transmit
  Vsci2rxd, // @ffc2  SCI2 receive
  Vsci2err, // @ffc4  SCI2 error
  Vsci1txd, // @ffc6  SCI1 transmit
  Vsci1txd, // @ffc8  SCI1 receive
  Vsci1err, // @ffca  SCI1 error
  Vsci0txd, // @ffcc  SCI0 transmit
  Vsci0rxd, // @ffce  SCI0 receive
  Vsci0err, // @ffd0  SCI0 error                
  Vadc,     // @ffd2  ADC             
  Vacmp,    // @ffd4  ACMP      
  Vmtim1,   // @ffd6  MTIM1       
  Vmtim0,   // @ffd8  MTIM0         
  Vftm0ovf, // @ffda  FTM0 overflow      
  Vftm0ch1, // @ffdc  FTM0 chan 1       
  Vftm0ch0, // @ffde  FTM0 chan 0         
  Vftm1ovf, // @ffe0  FTM1 overflow                
  Vftm1ch1, // @ffe2  FTM1 chan 1    
  Vftm1ch0, // @ffe4  FTM1 chan 0  
  Vftm2ovf, // @ffe6  FTM2 overflow 
  Vftm2ch5, // @ffe8  FTM2 chan 5
  Vftm2ch4, // @ffea  FTM2 chan 4
  Vftm2ch3, // @ffec  FTM2 chan 3
  Vftm2ch2, // @ffee  FTM2 chan 2   
  Vftm2ch1, // @fff0  FTM2 chan 1  
  Vftm2ch0, // @fff2  FTM2 chan 0    
  Vftm2flt, // @fff4  FTM2 fault  
  Vclk,     // @fff6  Clock loss of lock                
  Vlvw,     // @fff8  Low voltage warning 
  Virq,     // @fffa  IRQ                
  Vswi,     // @fffc  SWI              
  _stext    // @fffe  Reset              
  };
