/*****************************************************
*
* Firmware for the
* XL Bay Controller (2) Slave Processor.
*
* Tom Rebel
******************************************************/
#include <ctype.h>

#include  "portbits.h"
#include  "displayDriver.h"
#include  "SPICommunication.h"
#include  "globals.h"
#include  "diagState.h"
#include  "XLConstants.h"

unsigned char DTMHstate;

void
InitClk (void)
{
  
  // FLL in FEI mode
  ICS_C2 = 0x00;

  // internal clk =32.768kHz * 64 * N / R=32.768kHz * 64 * 18 / 1=37.748736MHz
  ICS_C1 = 0x04;
                                  
  //ICS_C2 = 0x40;    // busclk = 5 MHz (debug)
  ICS_C2 = 00;      // busclk = 20 MHz

  ICS_C3 = 0x50;    // trim value

  while(1) {
    if(ICS_S & 0x40)          // wait for FLL lock
      break;
  }

  if(ICS_S & 0x80)           // if lock loss, clear it
    ICS_S |= 0x80;
  
/*
  // clock in FEI mode (FFL enabled, internal clk source)
  ICS_C2 = 0x00;

  // internal reference clk to FLL
  ICS_C1 = 0x04;

  ICS_C2 = 0x00;    // Bdiv = 0, no prescale

  ICS_C3 = 0x50;    // trim value to yield 20 MHz bus clock (0x50 typical) use RTCO to tweak
*/  
  // enable bus clock to peripherals that will be used

  SCG_C1 = 0x01;    // RTC module
  //SCG_C2 = 0x20;    // debug module
  SCG_C3 = 0x0c;    // SPI 1 and SPI 0

}// InitClk


void
InitRTC (void)      // init RTC module to get 5 mS (200 Hz) periodic interrupt
{                   // assume bus clk = 20 MHz
  RTC_MODH = 0x06;  // 20 MHz / 64 = 312.5 KHz / 200 Hz = 1562.5 
  RTC_MODL = 0x1a;  // modulus value = 1562 (0x061a)
  
  RTC_SC2 = 0x87;   // RTC clk source = bus clk /1, prescale = 64
  RTC_SC1 = 0x50;   // enable interrupt and counter output

}// InitRTC

void
InitXLslave(void)
{
  unsigned char result;
  
  
  /* config pio's */
  PORT_PTAD = 0xff;                    // set PORT A outputs hi
  PORT_PTAOE = 0xdf;                   // config PORT A all outputs except A5
  
  PORT_PTBD = 0xff;                    // set PORT B outputs hi 
  PORT_PTBOE = 0xff;                   // config PORT B all outputs
  
  PORT_PTCD = 0xff;                    // set PORT C outputs hi
  PORT_PTCOE = 0xff;                   // config PORT C all outputs

  PORT_PTDD = 0xff;                    // set PORT D outputs hi
  PORT_PTDOE = 0xfb;                   // config PORT D all outputs except PD2
  PORT_PTDIE = 0x04;                   // PD2 = input (for when SPI port disabled)

  PORT_PTED = 0xff;                    // set PORT E outputs hi
  PORT_PTEOE = 0x7b;                   // config PORT E bits 0,1,3 - 6 to outputs 
  PORT_PTEIE = 0x84;                   // config PORT E bit 2 & 7 to input SendSlaveData
  PORT_PTEPE = 0x04;                    // enable pull up resistor on PE2
  
  PORT_PTHD = 0xff;                    // set PORT H outputs hi, (SlaveDataReadyN = 1)
  PORT_PTHOE = 0x04;                   // config PORT H bit 2 as an output SlaveDataReady
  
  SPI_Init();                 // init SPI ports (0 & 1)

  InitializeGlobals();
  
  ClearAllPorts();          // initialize dataspace to all possible LEDs off, then update stick LEDs

  result = Auto_Config_XL_Ports();  // determine what devices connected to each port (auto-config)
  if (result == FALSE)
    Auto_Config_XL_Ports();        // if 1st attempt fails, try it 1 more time
}// End Initialize_XL_Slave



// the rxSPIpacket queue contains packets received from the Master CPU via the SPI port 
void
DataFromMasterHandler (void)
{

  struct MiniPacket *bufptr;
  unsigned char portIdx, begLEDidx, endLEDidx, color, status;
  unsigned char i, nbrOfDevices, deviceType;

  bufptr = &rxSPIpacket[processSPIBufIdx];
  if (!(bufptr->status & PKT_RX_READ)) {
    return;  // buffer is empty (assume entire queue is empty)
  }
  
  switch (bufptr->data[0])    // 1st byte is the command
  {
    case CMD_DISPLAY_CLEAR:
      ClearAllPorts();
      break;
  
    case CMD_DIAGNOSTIC_STATE:
      diagnosticMode = TRUE;
      ActivateDiagState();
      break;
      
    case CMD_LED_RAM_UPDATE1:
      portIdx = bufptr->data[1];
      begLEDidx = bufptr->data[2];
      color = bufptr->data[3];
      status = bufptr->data[4];
      UpdateLEDMemory(portIdx, begLEDidx, color, status);
      break;

    case CMD_LED_RAM_UPDATEx:
      portIdx = bufptr->data[1];
      begLEDidx = bufptr->data[2];
      color = bufptr->data[3];
      status = bufptr->data[4];
      endLEDidx = bufptr->data[5];
      for (i = begLEDidx; i <= endLEDidx; i++)
        UpdateLEDMemory(portIdx, i, color, status);
      break;

    case CMD_PORT_UPDATE:
      portIdx = bufptr->data[1];
      SendPortLEDdata(portIdx, FALSE);
      break;

    case CMD_PORT_CLEAR:
      portIdx = bufptr->data[1];
      ClearPortLEDs(portIdx);
      break;

    case CMD_RUN_AUTOCONFIG:
      ClearAllPorts();    // init ram to all lights off, then update LEDs
      Auto_Config_XL_Ports();
      portToUpload = 0;   // initiate upload of config data to master CPU
      break;

    case CMD_INDICATE_DOWNLOAD:
      ClearAllPorts();
      downloadDisplayMode = TRUE;
      break;

    case CMD_UPDATE_ALL_PORTS:
      for (portIdx = 0; portIdx < MAX_XL_PORTS; portIdx++)
        if (XLConfigPort[portIdx].deviceCount > 0)
          SendPortLEDdata(portIdx, FALSE);
      break;

    case CMD_SET_PORT_DEVICES:
      portIdx = bufptr->data[1];
      nbrOfDevices = bufptr->data[2];
      deviceType = bufptr->data[3];
      XLConfigPort[portIdx].deviceCount = nbrOfDevices;
      for (i = 0; i < nbrOfDevices; i++) {
          XLConfigPort[portIdx].deviceType[i] = deviceType;
      }
      break;

    case CMD_COLOR_FILL_PORT:
      portIdx = bufptr->data[1];
      color = bufptr->data[3];
      status = bufptr->data[4];
      ColorFillPort(portIdx, color, status);
      break;

    default:
      break;
  }
  
  bufptr->status = PKT_RX_EMPTY;
  if (++processSPIBufIdx >= MAX_SPI_PACKETS)
    processSPIBufIdx = 0;

}// DataFromMasterHandler

void
DataToMasterHandler(void)
{
  unsigned char iport, device, deviceCount;


  switch (DTMHstate)
  {
    case 0:     // scan for button push (more precisely, RELEASED)
      SlaveDataReadyN = 1;      // inactive value 
      for (iport = 0; iport < MAX_XL_PORTS; iport++)
      {
        deviceCount = XLConfigPort[iport].deviceCount;
        for (device = 0; device < deviceCount; device++)
        {
          if (ButtonPort[iport].ButtonStick[device].Status == BUTTON_RELEASED)
          {
            SPItoMaster[0] = PKT_START_BYTE;
            SPItoMaster[1] = CMD_XLBUTTON;
            SPItoMaster[2] = iport;    // send port index
            SPItoMaster[3] = device;  // send device index
            SPItoMaster[4] = ButtonPort[iport].ButtonStick[device].Value;
            SPItoMaster[5] = PKT_STOP_BYTE;
            ButtonPort[iport].ButtonStick[device].Value = 0;
            ButtonPort[iport].ButtonStick[device].Status = BUTTON_CLEAR;
            DTMHstate = 2;
          } // end of if
        }  // end of for (device...
      } // end of for (iport...
      if (DTMHstate == 0) // if no buttons found pushed
        DTMHstate = 1;    // execute state 1 next time to check for port device upload request
      break;

    case 1:
      if (portToUpload < MAX_XL_PORTS)
      {
        deviceCount = XLConfigPort[portToUpload].deviceCount;
        SPItoMaster[0] = PKT_START_BYTE;
        SPItoMaster[1] = CMD_PORT_DEVICES;
        SPItoMaster[2] = portToUpload;    // send port index
        SPItoMaster[3] = XLConfigPort[portToUpload].deviceCount;
        for (device = 0; device < deviceCount; device++)
          SPItoMaster[device+4] = XLConfigPort[portToUpload].deviceType[device];
        SPItoMaster[device+4] = PKT_STOP_BYTE;
        portToUpload++;   // cycle through all porta
        DTMHstate = 2;
      }
      else  // no ports to upload, scan the buttons next time
        DTMHstate = 0;
      break;
    
    case 2:     // prepare to send buffer to Master
      SlaveDataReadyN = 0;    // inform master of data ready
      
      SPI_0_outputPtr = 0;
      DTMHstate = 3;           
      break;

    case 3:
      if (SendSlaveDataN == 0)  // advance when Master asserts SendSlaveData
        DTMHstate = 4;
      break;

    case 4:
      if (SendSlaveDataN == 1)   // loop til Master drops it's output
      {
        SlaveDataReadyN = 1;
        DTMHstate = 0;
      }
      break;

    default:
      DTMHstate = 0;
    
  } // end of switch
}


void main ()
{
  unsigned char first_loop = TRUE;
  unsigned char i, flashOnCtr, flashOffCtr, portIndex, displayCtr;

  //Initialize 
  //SYS_SOPT1 = 0x04;     //  reset pin enabled
  SYS_SOPT2 = 00;       // (note these are default settings after a reset)
  
  InitClk ();
  InitRTC();
  InitXLslave();
  flashOnCtr = 0;
  flashOffCtr = 0;
  portIndex = 0;
  displayCtr = 0;
  DTMHstate = 0;
  SPI_0_EnableReceiveInterrupt();


  _asm ("cli");      // enable interrupts

  if (PE2 == 0)   // if jumper installed at JP20
  {
    //-------------- debug in diagnostic mode -----------
    ActivateDiagState();
    diagnosticMode = TRUE;
    //downloadDisplayMode = TRUE;
    //---------------------------------------------------
  }
  

  while (1)
  {	
    DataFromMasterHandler();    // process commands from the master CPU
    
    if (diagnosticMode)
      DiagState();
    else
      DataToMasterHandler();      // check for data to upload to master
    
    if (last_rti_cnt == rti_ctr)  // rtc module interrupts every 5 mS
      continue;
    
    last_rti_cnt = rti_ctr;

    if (++portIndex >= MAX_XL_PORTS)
    {
      portIndex = 0;    // controls which port is being serviced during this 5 mS slice
      GenReadStrobe();  // load button SR's for all 10 ports
    }
    if (++displayCtr >= 200) { // 200 * 5mS = 1 second cycle
      displayCtr = 0;
      flashOffCtr = MAX_XL_PORTS;
      if (first_loop == TRUE)
      {
        first_loop = FALSE;
        portToUpload = 0;       // initiate upload of port config to master CPU after 1 second
      }
    } // end of every 1 sec chores
    else if (displayCtr == (250/5))
     flashOnCtr = MAX_XL_PORTS;
    
    // back to every 5 mS chores
      
    //if (!(SPI1CNTL1 & SPTIE))           // don't use SPI port if lights are being updated
    // using polled SPI output for now - change later  
    Read_XL_Device_Buttons(portIndex);  // read the buttons on 1 port
                                        // results in ButtonPort structure
    if (flashOnCtr) {
      BlinkPortLEDS(portIndex, FALSE);  // turn ON LEDs on Port (portIndex)
      flashOnCtr--;
    }

    if (flashOffCtr) {
      BlinkPortLEDS(portIndex, TRUE); // turn OFF LEDs on Port (portIndex) that
      flashOffCtr--;                  // have flash attribute
    }
    if (downloadDisplayMode)
      if ((displayCtr % 50) == 0)   // 50 * 5mS = 250 mS
        Download_Display();
      
    
  } // end of while (1)
} // end of main()

