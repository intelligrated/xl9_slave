/* INTERRUPTS.C
*
*  interrupt service routines are located here.
*
*/

#include "iospt60.h"
#include "globals.h"





//* -- iic -- *
@interrupt void Viic(void)
{ // interrupt due to iic comm
//INTR_2C++;
} // -- end iic()

//* -- atd -- *
@interrupt void Vadc(void)
{ // interrupt due to a/d converter
//INTR_atd++;
} // -- end atd()

//* -- keypad 1 -- *
@interrupt void Vkbi1(void)
{ // interrupt due to keypad
//INTR_keypad++;
} // -- end keypad()
//* -- keypad -- *

// -- keypad 0 --
@interrupt void Vkbi0(void)
{ // interrupt due to keypad
//INTR_keypad++;
} // -- end keypad()

//* -- uart2tx -- *
@interrupt void Vsci2txd(void)
{ // interrupt due to uart2 tx complete
//INTR_Uart2tx++;
} // -- end uart2tx()

//* -- uart2rx -- *
@interrupt void Vsci2rxd(void)
{ // interrupt due to uart2 rx
//INTR_Uart2rx++;
} // -- end uart2rx()

//* -- uart2err -- *
@interrupt void Vsci2err(void)
{ // interrupt due to uart2 rx errors
//INTR_Uart2err++;
} // -- end uart2err()

@interrupt void Vsci1txd(void)
{ // interrupt due to tx complete
//INTR_Uart1tx++;
} // end uart1tx()


@interrupt void Vsci1rxd(void)
{ // interrupt due to rx char ready
//INTR_Uart1rx++;
} // end uart1rx()


//* -- uart1err -- *
@interrupt void Vsci1err(void)
{ // interrupt due to uart1 rx
//INTR_Uart1err++;
} // -- end uart1err()

@interrupt void Vsci0txd(void)
{ // interrupt due to tx complete
//INTR_Uart1tx++;
} // end uart1tx()


@interrupt void Vsci0rxd(void)
{ // interrupt due to rx char ready
//INTR_Uart1rx++;
} // end uart1rx()


//* -- uart1err -- *
@interrupt void Vsci0err(void)
{ // interrupt due to uart1 rx
//INTR_Uart1err++;
} // -- end uart1err()

 
//* -- flex timer 2 ch 5 -- *
@interrupt void Vftm2ch5(void) 
{ 
  
  //TPM3SC &= (unsigned char) ~(0x80);  // read reg, then clear TOF (bit 7)
}

//* -- flex timer 2 ch 4 -- *
@interrupt void Vftm2ch4(void) 
{ 
  
  //TPM3SC &= (unsigned char) ~(0x80);  // read reg, then clear TOF (bit 7)
} 

//* -- flex timer 2 ch 3 -- *
@interrupt void Vftm2ch3(void)
{ 
//INTR_Timer3B++;
	//TPM3C1SC &= 0x7F;
} 

//* -- flex timer 2 ch 2 -- *
@interrupt void Vftm2ch2(void)
{ 
//INTR_Timer3A++;
	//TPM3C0SC &= 0x7F;
} 

//* -- flex timer 2 ch 1 -- *
@interrupt void Vftm2ch1(void)
{ 
  //TPM2SC = (unsigned char)(TPM2SC & 0x7f);  // clear the overflow flag
  //TPM2SC |= 0x40;     // enable interrupt
} 


//* -- flex timer 2 ch 0 -- *
@interrupt void Vftm2ch0(void)
{ // interrupt due to timer2E
//INTR_Timer2E++;
} 

//* -- flex timer 2 overflow -- *
@interrupt void Vftm2ovf(void)
{ 
//INTR_Timer2D++;
} 

//* -- flex timer 2 fault -- *
@interrupt void Vftm2flt(void)
{ 
//INTR_Timer1B++;
	//TPM1C1SC &= 0x7F;
} 

//* -- flex timer 1 ch 1 -- *
@interrupt void Vftm1ch1(void)
{ 
//INTR_Timer2C++;
} 

//* -- flex timer 1 ch 0 -- *
@interrupt void Vftm1ch0(void)
{ 
//INTR_Timer2B++;
	//TPM2C1SC &= 0x7F;
} 

//* -- flex timer 1 overflow -- *
@interrupt void Vftm1ovf(void)
{ 
//INTR_Timer1D++;
	//TPM1C3SC &= 0x7F;
}

//* -- flex timer 0 ch 1 -- *
@interrupt void Vftm0ch1(void)
{ 
//INTR_Timer2A++;
    //TPM2C0SC &= 0x7F; 
} 

//* -- flex timer 0 ch 0 -- *
@interrupt void Vftm0ch0(void) 
{ 
  //TPM1SC &= (unsigned char) ~(0x80);  
  
} 

//* -- flex timer 0 overflow -- *
@interrupt void Vftm0ovf(void)
{ 
//INTR_Timer1D++;
	//TPM1C3SC &= 0x7F;
} 
  

//* -- modulo timer 1 -- *
@interrupt void Vmtim1(void)
{ 
//INTR_Timer1A++;
    //TPM1C0SC &= 0x7F;
} 

//* -- modulo timer 0 -- *
@interrupt void Vmtim0(void)
{ 
//INTR_icg++;
} 

//* -- lvd -- *
@interrupt void Vlvw(void)
{ // interrupt due to low voltage warning
//INTR_lvd++;
} 

//* -- hwirq -- *
@interrupt void Virq(void)
{ // interrupt due to external hardware interrupt
//INTR_hwirq++;
} 

//* -- swi -- *
@interrupt void Vswi(void)
{ // interrupt due to software interrupt
//INTR_swi++;
} 

// ----- non-volatile memory ------
@interrupt void Vnvm(void)
{

}

// ----- Analog Comparator --------
@interrupt void Vacmp(void)
{

}

// ----- Touch Screen Interface -----
@interrupt void Vtsi(void)
{

}

// ----- loss of clock ---------
@interrupt void Vclk(void)
{

}

// ----- real time counter -----
@interrupt void Vrtc(void)
{
  rti_ctr++;          // increment real time interrupt counter
  RTC_SC1 |= 0X80;    // write a 1 to RTIF of RTC_SC1 to clear interrupt
}


 