/*	IO DEFINITIONS FOR MC9S08PT32, MC9S08PT60
 *	Copyright (c) 2012 by COSMIC Software
 */
#define uint	unsigned int

/*	PORTS section
 */
volatile char PORT_PTAD      @0x00;		/* port A data */
volatile char PORT_PTBD      @0x01;		/* port B data */
volatile char PORT_PTCD      @0x02;		/* port C data */
volatile char PORT_PTDD      @0x03;		/* port D data */
volatile char PORT_PTED      @0x04;		/* port E data */
volatile char PORT_PTFD      @0x05;		/* port F data */
volatile char PORT_PTGD      @0x06;		/* port G data */
volatile char PORT_PTHD      @0x07;		/* port H data */

/*	TOUCH SENSE INPUT section
 */
volatile char TSI_CS0        @0x08;		/* Control and Satus register 0 */
volatile char TSI_CS1        @0x09;		/* Control and Satus register 1 */
volatile char TSI_CS2        @0x0a;		/* Control and Satus register 2 */
volatile char TSI_CS3        @0x0b;		/* Control and Satus register 3 */
volatile char TSI_PEN0       @0x0c;		/* Pin Enable register 0 */
volatile char TSI_PEN1       @0x0d;		/* Pin Enable register 1 */
volatile uint TSI_CNT        @0x0e;		/* Count register */
volatile char TSI_CNTH       @0x0e;		/* Count register High */
volatile char TSI_CNTL       @0x0f;		/* Count register Low */

/*	ANALOG/DIGITAL section
 */
volatile char ADC_SC1        @0x10;		/* A/D status/ctrl register 1 */
volatile char ADC_SC2        @0x11;		/* A/D status/ctrl register 2 */
volatile char ADC_SC3        @0x12;		/* A/D status/ctrl register 3 */
volatile char ADC_SC4        @0x13;		/* A/D status/ctrl register 4 */
volatile char ADC_RH         @0x14;		/* A/D conversion result rgister high */
volatile char ADC_RL         @0x15;		/* A/D conversion result register low */
volatile char ADC_CVH        @0x16;		/* A/D compare value register high */
volatile char ADC_CVB        @0x17;		/* A/D compare value register low */

/*	MODULO TIMER 0 section
 */
volatile char MTIM0_SC       @0x18;		/* MTIM status/control register */
volatile char MTIM0_CLK      @0x19;		/* MTIM clock configuration register */
volatile char MTIM0_CNT      @0x1a;		/* MTIM counter register */
volatile char MTIM0_MOD      @0x1b;		/* MTIM modulo register */

/*	MODULO TIMER 1 section
 */
volatile char MTIM1_SC       @0x1c;		/* MTIM status/control register */
volatile char MTIM1_CLK      @0x1d;		/* MTIM clock configuration register */
volatile char MTIM1_CNT      @0x1e;		/* MTIM counter register */
volatile char MTIM1_MOD      @0x1f;		/* MTIM modulo register */

/*	FLEXTIMER 0 section
 */
volatile char FTM0_SC        @0x20;		/* FTM 0 status/ctrl reg */
volatile uint FTM0_CNT       @0x21;		/* FTM 0 counter register */
volatile char FTM0_CNTH      @0x21;		/* FTM 0 counter reg high */
volatile char FTM0_CNTL      @0x22;		/* FTM 0 counter reg low */
volatile uint FTM0_MOD       @0x23;		/* FTM 0 counter modulo reg */
volatile char FTM0_MODH      @0x23;		/* FTM 0 counter modulo high */
volatile char FTM0_MODL      @0x24;		/* FTM 0 counter modulo low */
volatile char FTM0_C0SC      @0x25;		/* FTM 0 chan 0 status/ctrl */
volatile uint FTM0_C0V       @0x26;		/* FTM 0 chan 0 value reg */
volatile char FTM0_C0VH      @0x26;		/* FTM 0 chan 0 value high */
volatile char FTM0_C0VL      @0x27;		/* FTM 0 chan 0 value low */
volatile char FTM0_C1SC      @0x28;		/* FTM 0 chan 1 status/ctrl */
volatile uint FTM0_C1V       @0x29;		/* FTM 0 chan 1 value reg */
volatile char FTM0_C1VH      @0x29;		/* FTM 0 chan 1 value high */
volatile char FTM0_C1VL      @0x2a;		/* FTM 0 chan 1 value low */

/*	ANALOG COMPARATOR section
 */
volatile char ACMP_CS        @0x2c;		/* CMP control/status register */
volatile char ACMP_C0        @0x2d;		/* CMP control register 0 */
volatile char ACMP_C1        @0x2e;		/* CMP control register 1 */
volatile char ACMP_C2        @0x2f;		/* CMP control register 2 */

/*	FLEXTIMER 1 section
 */
volatile char FTM1_SC        @0x30;		/* FTM 1 status/ctrl reg */
volatile uint FTM1_CNT       @0x31;		/* FTM 1 counter register */
volatile char FTM1_CNTH      @0x31;		/* FTM 1 counter reg high */
volatile char FTM1_CNTL      @0x32;		/* FTM 1 counter reg low */
volatile uint FTM1_MOD       @0x33;		/* FTM 1 counter modulo reg */
volatile char FTM1_MODH      @0x33;		/* FTM 1 counter modulo high */
volatile char FTM1_MODL      @0x34;		/* FTM 1 counter modulo low */
volatile char FTM1_C0SC      @0x35;		/* FTM 1 chan 0 status/ctrl */
volatile uint FTM1_C0V       @0x36;		/* FTM 1 chan 0 value reg */
volatile char FTM1_C0VH      @0x36;		/* FTM 1 chan 0 value high */
volatile char FTM1_C0VL      @0x37;		/* FTM 1 chan 0 value low */
volatile char FTM1_C1SC      @0x38;		/* FTM 1 chan 1 status/ctrl */
volatile uint FTM1_C1V       @0x39;		/* FTM 1 chan 1 value reg */
volatile char FTM1_C1VH      @0x39;		/* FTM 1 chan 1 value high */
volatile char FTM1_C1VL      @0x3a;		/* FTM 1 chan 1 value low */

/*	INTERRUPT section
 */
volatile char IRQ_SC         @0x3b;		/* IRQ status/control register */

/*	KEYBOARD section
 */
volatile char KBI0_SC        @0x3c;		/* Keyboard 0 status/control */
volatile char KBI1_SC        @0x3d;		/* Keyboard 1 status/control */

/*	IPC section
 */
volatile char IPC_SC         @0x3e;		/* IPC Status and Control register */
volatile char IPC_IPMPS      @0x3f;		/* Interrupt priority Mask Pseudo Stack reg */

/*	SYSTEM section
 */
volatile char SYS_SRS        @0x3000;	/* SYS reset status register */
volatile char SYS_SBDFR      @0x3001;	/* SYS background force reset */
volatile uint SYS_SDID       @0x3002;	/* SYS device id register */
volatile char SYS_SDIDH      @0x3002;	/* SYS device id register high */
volatile char SYS_SDIDL      @0x3003;	/* SYS device id register low */
volatile char SYS_SOPT1      @0x3004;	/* SYS options register 1 */
volatile char SYS_SOPT2      @0x3005;	/* SYS options register 2 */
volatile char SYS_SOPT3      @0x3006;	/* SYS options register 3 */
volatile char SYS_SOPT4      @0x3007;	/* SYS options register 4 */
volatile char SCG_C1         @0x300c;	/* SYS clock gating control reg 1 */
volatile char SCG_C2         @0x300d;	/* SYS clock gating control reg 2 */
volatile char SCG_C3         @0x300e;	/* SYS clock gating control reg 4 */
volatile char SCG_C4         @0x300f;	/* SYS clock gating control reg 4 */

/*	DEBUG section
 */
volatile uint DBG_CA         @0x3010;	/* DBG comparator A */
volatile char DBG_CAH        @0x3010;	/* DBG comparator A high */
volatile char DBG_CAL        @0x3011;	/* DBG comparator A low */
volatile uint DBG_CB         @0x3012;	/* DBG comparator B */
volatile char DBG_CBH        @0x3012;	/* DBG comparator B high */
volatile char DBG_CBL        @0x3013;	/* DBG comparator B low */
volatile uint DBG_CC         @0x3014;	/* DBG comparator C */
volatile char DBG_CCH        @0x3014;	/* DBG comparator C high */
volatile char DBG_CCL        @0x3015;	/* DBG comparator C low */
volatile uint DBG_F          @0x3016;	/* DBG fifo register */
volatile char DBG_FH         @0x3016;	/* DBG fifo register high */
volatile char DBG_FL         @0x3017;	/* DBG fifo register low */
volatile char DBG_CAX        @0x3018;	/* DBG comparator A extention */
volatile char DBG_CBX        @0x3019;	/* DBG comparator B extention */
volatile char DBG_CCX        @0x301a;	/* DBG comparator C extention */
volatile char DBG_FX         @0x301b;	/* DBG FIFO extended information */
volatile char DBG_C          @0x301c;	/* DBG control register */
volatile char DBG_T          @0x301d;	/* DBG trigger register */
volatile char DBG_S          @0x301e;	/* DBG status register */
volatile char DBG_CNT        @0x301f;	/* DBG counter */

/*	FLASH section
 */
volatile char NVM_FCLKCDIV   @0x3020;	/* FLASH clock divider */
volatile char NVM_FSEC       @0x3021;	/* FLASH security register */
volatile char NVM_FCCOBIX    @0x3022;	/* FLASH CCOB index register */
volatile char NVM_FCNFG      @0x3024;	/* FLASH configuration register */
volatile char NVM_FERCNFG    @0x3025;	/* FLASH error config. register */
volatile char NVM_FSTAT      @0x3026;	/* FLASH status register */
volatile char NVM_FERSTAT    @0x3027;	/* FLASH error status register */
volatile char NVM_FPROT      @0x3028;	/* FLASH protection register */
volatile char NVM_EEPROT     @0x3029;	/* EEPROM protection register */
volatile uint NVM_FCCOB      @0x302a;	/* FLASH CCOB register */
volatile char NVM_FCCOBHI    @0x302a;	/* FLASH CCOB register high */
volatile char NVM_FCCOBLO    @0x302b;	/* FLASH CCOB register low */
volatile char NVM_FOPT       @0x302c;	/* FLASH options register */

/*	WATCHDOG section
 */
volatile char WDOG_CS1       @0x3030;	/* WDOG Ctrl/Status register 1 */
volatile char WDOG_CS2       @0x3031;	/* WDOG Ctrl/Status register 2 */
volatile uint WDOG_CNT       @0x3032;	/* WDOG Counter register */
volatile char WDOG_CNTH      @0x3032;	/* WDOG Counter register high */
volatile char WDOG_CNTL      @0x3033;	/* WDOG Counter register low */
volatile uint WDOG_TOVAL     @0x3034;	/* WDOG Timout Value register */
volatile char WDOG_TOVALH    @0x3034;	/* WDOG Timout Value register */
volatile char WDOG_TOVALL    @0x3035;	/* WDOG Timout Value register */
volatile uint WDOG_WIN       @0x3036;	/* WDOG Window register */
volatile char WDOG_WINH      @0x3036;	/* WDOG Window register high */
volatile char WDOG_WINL      @0x3037;	/* WDOG Window register low */

/*	INTERNAL CLOCK section
 */
volatile char ICS_C1         @0x3038;	/* ICS control register 1 */
volatile char ICS_C2         @0x3039;	/* ICS control register 2 */
volatile char ICS_C3         @0x303a;	/* ICS control register 3 */
volatile char ICS_C4         @0x303b;	/* ICS control register 4 */
volatile char ICS_S          @0x303c;	/* ICS status register */
volatile char ICS_OSCSC      @0x303e;	/* ICS status/control register */

/*	SYSTEM section1
 */
volatile char PMC_SPMSC1     @0x3040;	/* SYS power stat/ctrl reg 1 */
volatile char PMC_SPMSC2     @0x3041;	/* SYS power stat/ctrl reg 2 */
volatile char SYS_ILLAH      @0x304a;	/* SYS Illegal Address Register high */
volatile char SYS_ILLAL      @0x304b;	/* SYS Illegal Address Register low */

/*	INTERRUPT PRIORITY CONTROL section
 */
volatile char IPC_ILRS0      @0x3050;	/* Interrupt Level setting reg 0 */
volatile char IPC_ILRS1      @0x3051;	/* Interrupt Level setting reg 1 */
volatile char IPC_ILRS2      @0x3052;	/* Interrupt Level setting reg 2 */
volatile char IPC_ILRS3      @0x3053;	/* Interrupt Level setting reg 3 */
volatile char IPC_ILRS4      @0x3054;	/* Interrupt Level setting reg 4 */
volatile char IPC_ILRS5      @0x3055;	/* Interrupt Level setting reg 5 */
volatile char IPC_ILRS6      @0x3056;	/* Interrupt Level setting reg 6 */
volatile char IPC_ILRS7      @0x3057;	/* Interrupt Level setting reg 7 */
volatile char IPC_ILRS8      @0x3058;	/* Interrupt Level setting reg 8 */
volatile char IPC_ILRS9      @0x3059;	/* Interrupt Level setting reg 9 */

/*	CYCLIC REDUNDANCY CHECK section
 */
volatile char CRC_D0         @0x3060;	/* CRC register reg 0 */
volatile char CRC_D1         @0x3061;	/* CRC register reg 1 */
volatile char CRC_D2         @0x3062;	/* CRC register reg 2 */
volatile char CRC_D3         @0x3063;	/* CRC register reg 3 */
volatile char CRC_P0         @0x3064;	/* CRC polynomial reg 0 */
volatile char CRC_P1         @0x3065;	/* CRC polynomial reg 1 */
volatile char CRC_P2         @0x3066;	/* CRC polynomial reg 2 */
volatile char CRC_P3         @0x3067;	/* CRC polynomial reg 3 */
volatile char CRC_CTRL       @0x3068;	/* CRC control register */

/*	REAL TIME COUNTER section
 */
volatile char RTC_SC1        @0x306a;	/* RTC status and control reg 1 */
volatile char RTC_SC2        @0x306b;	/* RTC status and control reg 2 */
volatile uint RTC_MOD        @0x306c;	/* RTC modulo register */
volatile char RTC_MODH       @0x306c;	/* RTC modulo register high */
volatile char RTC_MODL       @0x306d;	/* RTC modulo register low */
volatile uint RTC_CNT        @0x306e;	/* RTC counter register */
volatile char RTC_CNTH       @0x306e;	/* RTC counter register high */
volatile char RTC_CNTL       @0x306f;	/* RTC counter register low */

/*	IIC section
 */
volatile char I2C_A1         @0x3070;	/* IIC address register */
volatile char I2C_F          @0x3071;	/* IIC frequency divide register */
volatile char I2C_C1         @0x3072;	/* IIC control register 1 */
volatile char I2C_S          @0x3073;	/* IIC status register */
volatile char I2C_D          @0x3074;	/* IIC data register */
volatile char I2C_C2         @0x3075;	/* IIC control register 2 */
volatile char I2C_FLT        @0x3076;	/* IIC Programmable Input Glitch Filer */
volatile char I2C_RA         @0x3077;	/* IIC Range Address register */
volatile char I2C_SMB        @0x3078;	/* IIC SMBus control and status reg */
volatile char I2CA_2         @0x3079;	/* IIC address register 2 */
volatile uint I2C_SLT        @0x307a;	/* IIC SCL Low Time Out register */
volatile char I2C_SLTH       @0x307a;	/* IIC SCL Low Time Out reg high */
volatile char I2C_SLTL       @0x307b;	/* IIC SCL Low Time Out reg low */

/*	KEYBOARD section
 */
volatile char KBI0_PE        @0x307c;	/* Keyboard pin select reg 0 */
volatile char KBI0_ES        @0x307d;	/* Keyboard pin edge select reg 0 */
volatile char KBI1_PE        @0x307e;	/* Keyboard pin select reg 1 */
volatile char KBI1_ES        @0x307f;	/* Keyboard pin edge select reg 1 */

/*	SCI 0 section
 */
volatile uint SCI0_BD        @0x3080;	/* SCI 0 baud rate */
volatile char SCI0_BDH       @0x3080;	/* SCI 0 baud rate high */
volatile char SCI0_BDL       @0x3081;	/* SCI 0 baud rate low */
volatile char SCI0_C1        @0x3082;	/* SCI 0 control register 1 */
volatile char SCI0_C2        @0x3083;	/* SCI 0 control register 2 */
volatile char SCI0_S1        @0x3084;	/* SCI 0 status register 1 */
volatile char SCI0_S2        @0x3085;	/* SCI 0 status register 2 */
volatile char SCI0_C3        @0x3086;	/* SCI 0 control register 3 */
volatile char SCI0_D         @0x3087;	/* SCI 0 data register */

/*	SCI 1 section
 */
volatile uint SCI1_BD        @0x3088;	/* SCI 1 baud rate */
volatile char SCI1_BDH       @0x3088;	/* SCI 1 baud rate high */
volatile char SCI1_BDL       @0x3089;	/* SCI 1 baud rate low */
volatile char SCI1_C1        @0x308a;	/* SCI 1 control register 1 */
volatile char SCI1_C2        @0x308b;	/* SCI 1 control register 2 */
volatile char SCI1_S1        @0x308c;	/* SCI 1 status register 1 */
volatile char SCI1_S2        @0x308d;	/* SCI 1 status register 2 */
volatile char SCI1_C3        @0x308e;	/* SCI 1 control register 3 */
volatile char SCI1_D         @0x308f;	/* SCI 1 data register */

/*	SCI 2 section
 */
volatile uint SCI2_BD        @0x3090;	/* SCI 2 baud rate */
volatile char SCI2_BDH       @0x3090;	/* SCI 2 baud rate high */
volatile char SCI2_BDL       @0x3091;	/* SCI 2 baud rate low */
volatile char SCI2_C1        @0x3092;	/* SCI 2 control register 1 */
volatile char SCI2_C2        @0x3093;	/* SCI 2 control register 2 */
volatile char SCI2_S1        @0x3094;	/* SCI 2 status register 1 */
volatile char SCI2_S2        @0x3095;	/* SCI 2 status register 2 */
volatile char SCI2_C3        @0x3096;	/* SCI 2 control register 3 */
volatile char SCI2_D         @0x3097;	/* SCI 2 data register */

/*	SPI 0 section
 */
volatile char SPI0_C1        @0x3098;	/* SPI 0 control register 1 */
volatile char SPI0_C2        @0x3099;	/* SPI 0 control register 2 */
volatile char SPI0_BR        @0x309a;	/* SPI 0 baud rate register */
volatile char SPI0_S         @0x309b;	/* SPI 0 status register */
volatile char SPI0_D         @0x309d;	/* SPI 0 data register */
volatile char SPI0_MR        @0x309f;	/* SPI 0 match register */

/*	SPI 1 section
 */
volatile char SPI1_C1        @0x30a0;	/* SPI 1 control register 1 */
volatile char SPI1_C2        @0x30a1;	/* SPI 1 control register 2 */
volatile char SPI1_BR        @0x30a2;	/* SPI 1 baud rate register */
volatile char SPI1_S         @0x30a3;	/* SPI 1 status register */
volatile uint SPI1_D         @0x30a4;	/* SPI 1 data register */
volatile char SPI1_DH        @0x30a4;	/* SPI 1 data register high */
volatile char SPI1_DL        @0x30a5;	/* SPI 1 data register low */
volatile uint SPI1_M         @0x30a6;	/* SPI 1 match register */
volatile char SPI1_MH        @0x30a6;	/* SPI 1 match register high */
volatile char SPI1_ML        @0x30a7;	/* SPI 1 match register low */
volatile char SPI1_C3        @0x30a8;	/* SPI 1 control register 3 */
volatile char SPI1_CI        @0x30a9;	/* SPI 1 Clear Interrupt register */

/*	ANALOG/DIGITAL section
 */
volatile char ADC_APCTL1     @0x30ac;	/* A/D pin control register 1 */
volatile char ADC_APCTL2     @0x30ad;	/* A/D pin control register 2 */

/*	PARALLEL PORT section
 */
volatile char PORT_HDRVE     @0x30af;	/* PORT High Drive Enable reg */
volatile char PORT_PTAOE     @0x30b0;	/* PORT A Output Enable reg */
volatile char PORT_PTBOE     @0x30b1;	/* PORT B Output Enable reg */
volatile char PORT_PTCOE     @0x30b2;	/* PORT C Output Enable reg */
volatile char PORT_PTDOE     @0x30b3;	/* PORT D Output Enable reg */
volatile char PORT_PTEOE     @0x30b4;	/* PORT E Output Enable reg */
volatile char PORT_PTFOE     @0x30b5;	/* PORT F Output Enable reg */
volatile char PORT_PTGOE     @0x30b6;	/* PORT G Output Enable reg */
volatile char PORT_PTHOE     @0x30b7;	/* PORT H Output Enable reg */
volatile char PORT_PTAIE     @0x30b8;	/* PORT A Input Enable reg */
volatile char PORT_PTBIE     @0x30b9;	/* PORT B Input Enable reg */
volatile char PORT_PTCIE     @0x30ba;	/* PORT C Input Enable reg */
volatile char PORT_PTDIE     @0x30bb;	/* PORT D Input Enable reg */
volatile char PORT_PTEIE     @0x30bc;	/* PORT E Input Enable reg */
volatile char PORT_PTFIE     @0x30bd;	/* PORT F Input Enable reg */
volatile char PORT_PTGIE     @0x30be;	/* PORT G Input Enable reg */
volatile char PORT_PTHIE     @0x30bf;	/* PORT H Input Enable reg */
volatile char PORT_IOFLT0    @0x30ec;	/* PORT Filter reg 0 */
volatile char PORT_IOFLT1    @0x30ed;	/* PORT Filter reg 1 */
volatile char PORT_IOFLT2    @0x30ee;	/* PORT Filter reg 2 */
volatile char PORT_FCLKDIV   @0x30ef;	/* PORT Clock Division reg */

/*      FLEXTIMER 2 section
 */
volatile char FTM2_SC        @0x30c0;	/* FTM 2 status/ctrl reg */
volatile uint FTM2_CNT       @0x30c1;	/* FTM 2 counter register */
volatile char FTM2_CNTH      @0x30c1;	/* FTM 2 counter high */
volatile char FTM2_CNTL      @0x30c2;	/* FTM 2 counter low */
volatile uint FTM2_MOD       @0x30c3;	/* FTM 2 counter modulo register */
volatile char FTM2_MODH      @0x30c3;	/* FTM 2 counter modulo high */
volatile char FTM2_MODL      @0x30c4;	/* FTM 2 counter modulo low */
volatile char FTM2_C0SC      @0x30c5;	/* FTM 2 chan 0 status/ctrl */
volatile uint FTM2_C0V       @0x30c6;	/* FTM 2 chan 0 value register */
volatile char FTM2_C0VH      @0x30c6;	/* FTM 2 chan 0 value high */
volatile char FTM2_C0VL      @0x30c7;	/* FTM 2 chan 0 value low */
volatile char FTM2_C1SC      @0x30c8;	/* FTM 2 chan 1 status/ctrl */
volatile uint FTM2_C1V       @0x30c9;	/* FTM 2 chan 1 value register */
volatile char FTM2_C1VH      @0x30c9;	/* FTM 2 chan 1 value high */
volatile char FTM2_C1VL      @0x30ca;	/* FTM 2 chan 1 value low */
volatile char FTM2_C2SC      @0x30cb;	/* FTM 2 chan 2 status/ctrl */
volatile uint FTM2_C2V       @0x30cc;	/* FTM 2 chan 2 value register */
volatile char FTM2_C2VH      @0x30cc;	/* FTM 2 chan 2 value high */
volatile char FTM2_C2VL      @0x30cd;	/* FTM 2 chan 2 value low */
volatile char FTM2_C3SC      @0x30ce;	/* FTM 2 chan 3 status/ctrl */
volatile uint FTM2_C3V       @0x30cf;	/* FTM 2 chan 3 value register */
volatile char FTM2_C3VH      @0x30cf;	/* FTM 2 chan 3 value high */
volatile char FTM2_C3VL      @0x30d0;	/* FTM 2 chan 3 value low */
volatile char FTM2_C4SC      @0x30d1;	/* FTM 2 chan 4 status/ctrl */
volatile uint FTM2_C4V       @0x30d2;	/* FTM 2 chan 4 value register */
volatile char FTM2_C4VH      @0x30d2;	/* FTM 2 chan 4 value high */
volatile char FTM2_C4VL      @0x30d3;	/* FTM 2 chan 4 value low */
volatile char FTM2_C5SC      @0x30d4;	/* FTM 2 chan 5 status/ctrl */
volatile uint FTM2_C5V       @0x30d5;	/* FTM 2 chan 5 value register */
volatile char FTM2_C5VH      @0x30d5;	/* FTM 2 chan 5 value high */
volatile char FTM2_C5VL      @0x30d6;	/* FTM 2 chan 5 value low */
volatile uint FTM2_CNTIN     @0x30d7;	/* FTM 2 Counter Initial value register */
volatile char FTM2_CNTINH    @0x30d7;	/* FTM 2 Counter Initial value high */
volatile char FTM2_CNTINL    @0x30d8;	/* FTM 2 Counter Initial value low */
volatile char FTM2_STATUS    @0x30d9;	/* FTM 2 Capture and Compare Status reg */
volatile char FTM2_MODE      @0x30da;	/* FTM 2 Features mode Selection reg */
volatile char FTM2_SYNC      @0x30db;	/* FTM 2 Synchronization register */
volatile char FTM2_OUTINIT   @0x30dc;	/* FTM 2 Initial State for Channel Output reg */
volatile char FTM2_OUTMASK   @0x30dd;	/* FTM 2 Output Mask register */
volatile char FTM2_COMBINE0  @0x30de;	/* FTM 2 Function for Linked Channels reg 0 */
volatile char FTM2_COMBINE1  @0x30df;	/* FTM 2 Function for Linked Channels reg 1 */
volatile char FTM2_COMBINE2  @0x30e0;	/* FTM 2 Function for Linked Channels reg 2 */
volatile char FTM2_COMBINE3  @0x30e1;	/* FTM 2 Function for Linked Channels reg 3 */
volatile char FTM2_DEADTIME  @0x30e2;	/* FTM 2 Deadtime Insertion Control reg */
volatile char FTM2_EXTTRIG   @0x30e3;	/* FTM 2 External Trigger register */
volatile char FTM2_POL       @0x30e4;	/* FTM 2 Channels polarity register */
volatile char FTM2_FMS       @0x30e5;	/* FTM 2 Fault Mode Status register */
volatile char FTM2_FILTER0   @0x30e6;	/* FTM 2 Input Capture Filter Control reg 0 */
volatile char FTM2_FILTER1   @0x30e7;	/* FTM 2 Input Capture Filter Control reg 1 */
volatile char FTM2_FLTFILTER @0x30e8;	/* FTM 2 Fault Input Filter Control register */
volatile char FTM2_FLTCTRL   @0x30e9;	/* FTM 2 Fault Control register */

/*	PORT PIN section
 */
volatile char PORT_PTAPE     @0x30f0;	/* PORT A pullup enable */
volatile char PORT_PTBPE     @0x30f1;	/* PORT B pullup enable */
volatile char PORT_PTCPE     @0x30f2;	/* PORT C pullup enable */
volatile char PORT_PTDPE     @0x30f3;	/* PORT D pullup enable */
volatile char PORT_PTEPE     @0x30f4;	/* PORT E pullup enable */
volatile char PORT_PTFPE     @0x30f5;	/* PORT F pullup enable */
volatile char PORT_PTGPE     @0x30f6;	/* PORT G pullup enable */
volatile char PORT_PTHPE     @0x30f7;	/* PORT H pullup enable */

/*	Universally Unique Identification section
 */
volatile char SYS_UUID1      @0x30f8;	/* Universally Unique Identification reg 1 */
volatile char SYS_UUID2      @0x30f9;	/* Universally Unique Identification reg 2 */
volatile char SYS_UUID3      @0x30fa;	/* Universally Unique Identification reg 3 */
volatile char SYS_UUID4      @0x30fb;	/* Universally Unique Identification reg 4 */
volatile char SYS_UUID5      @0x30fc;	/* Universally Unique Identification reg 5 */
volatile char SYS_UUID6      @0x30fd;	/* Universally Unique Identification reg 6 */
volatile char SYS_UUID7      @0x30fe;	/* Universally Unique Identification reg 7 */
volatile char SYS_UUID8      @0x30ff;	/* Universally Unique Identification reg 2 */

/*	NON VOLATILE section
 */
volatile char NV_BACKKEY[8]  @0xff70;	/* NV comparison key */
volatile char NV_FPROT       @0xff7c;	/* NV protection register */
volatile char NV_EEPROT      @0xff7d;	/* NV Eeprom protection register */
volatile char NV_OPT         @0xff7e;	/* NV options register */
volatile char NV_FSEC        @0xff7f;	/* NV Flash Security register */

#undef uint
