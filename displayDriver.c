//
// filename: displayDriver.c
// Author: tom rebel
// 6/30/2016
//


////////////

// Headers
////////////

#include "portBits.h"
#include "string.h"
#include "XLConstants.h"
#include "SPIcommunication.h"
#include "structs.h"
#include "globals.h"
#include "TrakConstants.h"







extern unsigned char SPIbuffer[];



// **************************************************************** 
//
// Auto_Config_XL_Ports      
// Reads all XL ports to determine how many and what type of XL devices
//       are present. Stores these values in the XLConfigPort[] array
// Returns: TRUE if 2 successive scans yielded identical results
//          FALSE if 10 attempts could not yield identical results
// **************************************************************** 
unsigned char 
Auto_Config_XL_Ports(void)
{
  unsigned char iport, devNum, attempts, goodVerifyScan;
  union     xlButtons buttons;

  GenReadStrobe();       // load button shift reg (necessary for new stick design)
						  // all 10 ports
  for (iport = 0; iport < MAX_XL_PORTS; iport++)
  {
	ReadyPortForSPIRead(iport); // config SPI port & enable appropriate SPI buffers
	pollSPI_1_Read(MAX_XL_BUTTONLIGHTS_PER_PORT);   // read max XL devices per port
	DisableSPI_ReadWrite();   // deselect all ports

	for (devNum = 0; devNum < MAX_XL_BUTTONLIGHTS_PER_PORT; devNum++) // assuming max # button lights > max # of stick lights 
	{                                                           
	  buttons.c = SPIbuffer[devNum];

	  if (buttons.b.stickPresent1 != 1) // check for valid stick ID
		break;

	  if ((buttons.b.XLdeviceID == XL_BL_1_BUTTON)      ||
		  (buttons.b.XLdeviceID == XL_BL_2_BUTTON)      ||
		  (buttons.b.XLdeviceID == XL_12STICK_W_BUTTONS)  ||
		  (buttons.b.XLdeviceID == XL_12STICK_WO_BUTTONS) ||
          (buttons.b.XLdeviceID == XL_9STICK_W_BUTTONS)    ||
          (buttons.b.XLdeviceID == XL_9STICK_WO_BUTTONS)) 
	  {
		XLConfigPort[iport].deviceType[devNum] = buttons.b.XLdeviceID;
	  }
	  else
		break;
      
	}
	if ((devNum == MAX_XL_BUTTONLIGHTS_PER_PORT) || (buttons.c == 0xff)) // reading one past the last stick yields 0xff
	  XLConfigPort[iport].deviceCount = devNum;
  } // end of for (iport ...)

  attempts = 10;

  while (attempts > 0)    // 2 successive scans must yield identical results
  {
	goodVerifyScan = TRUE;  // presume success on verify scan
    GenReadStrobe();       // load button shift registers (all 10 ports)
	for (iport = 0; iport < MAX_XL_PORTS; iport++)
	{
	  ReadyPortForSPIRead(iport); // config SPI port & enable appropriate buffers
	  pollSPI_1_Read(MAX_XL_BUTTONLIGHTS_PER_PORT);   // read max XL devices per port
	  DisableSPI_ReadWrite();   // deselect all ports

	  for (devNum = 0; devNum < MAX_XL_BUTTONLIGHTS_PER_PORT; devNum++) // assuming max # button lights > max # of stick lights
	  {                                                           
		buttons.c = SPIbuffer[devNum];
        
		if (buttons.b.stickPresent1 != 1) // check for valid stick ID
        {
          goodVerifyScan == FALSE;
		  break;
        }
		if ((buttons.b.XLdeviceID == XL_BL_1_BUTTON)      ||
		  (buttons.b.XLdeviceID == XL_BL_2_BUTTON)      ||
		  (buttons.b.XLdeviceID == XL_12STICK_W_BUTTONS)  ||
		  (buttons.b.XLdeviceID == XL_12STICK_WO_BUTTONS) ||
          (buttons.b.XLdeviceID == XL_9STICK_W_BUTTONS)   ||
          (buttons.b.XLdeviceID == XL_9STICK_WO_BUTTONS)) 
		{
		  //XLConfigPort[iport].verifyDeviceType[istick] = buttons.b.XLdeviceID;
          if (XLConfigPort[iport].deviceType[devNum] != buttons.b.XLdeviceID)
          {
            XLConfigPort[iport].deviceType[devNum] = buttons.b.XLdeviceID;
            goodVerifyScan = FALSE;
          }
		}
		else
		 break;
	  } // end of for (devNum ...)
	  if ((devNum == MAX_XL_BUTTONLIGHTS_PER_PORT) || (buttons.c == 0xff)) // reading one past the last stick yields 0xff
		if (XLConfigPort[iport].deviceCount != devNum)
        {
          XLConfigPort[iport].deviceCount = devNum;
          goodVerifyScan = FALSE;
        }
       
	} // end of for (iport ...)

    if (goodVerifyScan)
    {
      return(TRUE);
    }
	attempts--;
  }   // end of while(attempts)
  return(FALSE);  // 2 successive scans never matched, return showing failure

} // End Auto_Config_XL_Ports

// **************************************************************** 
//
// Read_XL_Device_Buttons
// Reads the button inputs on the specified XL port using the SPI port.
//   Works with XL12 stick lights and Button Lights
// inputs: iport = XL port to read, range 0 to MAX_XL_PORTS-1
//                        
// **************************************************************** 
void 
Read_XL_Device_Buttons(unsigned char iport)
{
  unsigned char devNum, devCount;
  union     xlButtons buttons;

  devCount = XLConfigPort[iport].deviceCount;

  if (devCount > 0)
  {
    ReadyPortForSPIRead(iport);   // config SPI port & enable appropriate buffers
    pollSPI_1_Read(devCount);       // 1 read BYTE per device
    DisableSPI_ReadWrite();       // deselect all ports

    for (devNum = 0; devNum < devCount; devNum++) // compare with last scan to determine button change of states
    { 
  	  if (XLConfigPort[iport].deviceType[devNum] == XL_12STICK_WO_BUTTONS)
	    continue;   // no buttons on this device, goto next device
      if (XLConfigPort[iport].deviceType[devNum] == XL_9STICK_WO_BUTTONS)
        continue;

	  buttons.c = SPIbuffer[devNum];   // buttons is a union of bit field structures
	  if (buttons.b.stickPresent1 != 1)  // check for valid device ID
	    continue;
        
	  if (buttons.b.XLdeviceID != XLConfigPort[iport].deviceType[devNum])
	    continue;                       // skip this device if device ID != auto-config value
      
      if (buttons.b.XLdeviceID == XL_9STICK_W_BUTTONS)
        buttons.c |= 0x08;    // set bit 3 to make grounded input = logic 1 (no button push)  
	  
      buttons.c <<= 1;      // shift left once so that all button bits are in the msnibble                
 
	  buttons.n.n2 = (unsigned char) ~buttons.n.n2;   // the hardware design results in 1 means released, 0 means pressed    
														  // negate the input value to get the opposite logic
	  switch (ButtonPort[iport].ButtonStick[devNum].Status)
	  {
	    case BUTTON_CLEAR:    // CLEAR means all button RELEASEs reported to controller
	   	  switch (buttons.n.n2)
		  {
		    case 1:     // only single button pushes are valid
		    case 2:
		    case 4:
		    case 8:
		    case 12:     // legal with 1 switch button lights
		  	  if (ButtonPort[iport].ButtonStick[devNum].Value != buttons.n.n2) // 2 consecutive scans required
			  {                                                           // for a valid button push
			    ButtonPort[iport].ButtonStick[devNum].Value = buttons.n.n2;
			  }
			  else  // value register = newly scanned data 
			  {
			    switch (buttons.n.n2) // convert input data to logical button values
			    {
			   	  case 1:
				    ButtonPort[iport].ButtonStick[devNum].Value = S4_BUTTON;
				    break;
				  case 2:
				    ButtonPort[iport].ButtonStick[devNum].Value = S3_BUTTON;
				    break;
				  case 4:
				    ButtonPort[iport].ButtonStick[devNum].Value = S2_BUTTON;
				    break;
				  case 8:
				  case 12:    // 1 switch button light
				    ButtonPort[iport].ButtonStick[devNum].Value = S1_BUTTON;
				    break;
				}
				ButtonPort[iport].ButtonStick[devNum].Status = BUTTON_PUSHED;    // PUSHes are not reported; RELEASEs are
		      }
			  break;
			default:  // current button scan illegal
			  ButtonPort[iport].ButtonStick[devNum].Value = 0;
                
          }
		  break;
      
		case BUTTON_PUSHED:
		  if (!buttons.n.n2) // if no buttons pushed
		    ButtonPort[iport].ButtonStick[devNum].Status = BUTTON_RELEASED;   // nothing now pushed, so change status to RELEASED 
		  break;                                                        // note that buttonValue still contains button(s) that were PUSHED
          
		default: //case BUTTON_RELEASED:
		  ButtonPort[iport].ButtonStick[devNum].Value = 0;       // unlikely to ever happen since RELEASES are reported immediately to the controller
		  ButtonPort[iport].ButtonStick[devNum].Status = BUTTON_CLEAR; // see ConfiguredState.c
	  }
	} // end of for (istick = 0; ...
  }  // end of if (Shelf[iport] ...
} // End of Read_XL12_Buttons

// -------------------------------------------------------------------------------------------------------- 
//
// UpdateLEDMemory(unsigned char portIdx, unsigned char ledIdx, unsigned char ledColor, unsigned char ledStatus)
//   Updates the memory location for the one specified RGBLED to the specified color 
//   Memory is formatted for output to the SPI port, for quick updates  
//     Inputs:  portIdx range 0 -> (MAX_XL_PORTS - 1)
//              ledColor format: logic 1 turns on the led, b0: blue, b1: green, b2: red
//             ledIdx range: 0 to (# of stick on port * 12) - 1
//             ledStatus: LT_ON, LT_OFF, LT_BLINKING (see  trakConstants.h)
//     Returns: FALSE if ledNum > max for specified port or port > max
//              TRUE otherwise
//     SPI port format for stick lights: (pattern repeats every 3 bytes) logic 0 turns on LED
//     B7                   B0 | B7                   B0 | B7                   B0 |
//     R0 G0 B0 R1 G1 B1 R2 G2 | B2 R3 G3 B3 R4 G4 B4 R5 | G5 B5 R6 G6 B6 R7 G7 B7 |
// -------------------------------------------------------------------------------------------------------- 
//     SPI port format for button lights: (1 byte per buttonlight)
//     B7                     B0  |
//     R0 G0 B0 R1 G1 B1 TL0 TL1 |
//--------------------------------------------------------------------------------------------
// R -> Red LED    G -> Green LED    B -> Blue LED     TL -> tail light
//--------------------------------------------------------------------------------------------
unsigned char 
UpdateLEDMemory(unsigned char portIdx, unsigned char ledIdx, unsigned char ledColor, unsigned char ledStatus)
{
  unsigned char d8, m8, baseIdx, uc, ledNumEvenOdd;

  if (portIdx >= MAX_XL_PORTS)
	return(FALSE);

  ledColor = (unsigned char)(ledColor ^ 0x07);    // invert the 3 lsbits so that a received logic 1 turns on the LED
  if (ledStatus == LT_OFF)								// hardware requires a logic 0 to turn on LED
    ledColor = 0x07;    // status trumps color when it is LT_OFF

  if ((XLConfigPort[portIdx].deviceType[0] == XL_BL_1_BUTTON) ||   // an XL port will contain either all button lights OR all stick lights
      (XLConfigPort[portIdx].deviceType[0] == XL_BL_2_BUTTON))       // check device Type of 1st device on port
  {
    baseIdx = (unsigned char)(ledIdx/2);   // for button lights, each byte contains 2 RGB data and 2 tail light data
    ledNumEvenOdd = (unsigned char)(ledIdx % 2);    // even: 0 (b7 - b5)   odd: 1 (b4 - b2)
    uc = XLport[portIdx].LightData[baseIdx];  // get old data
    if (ledNumEvenOdd == 0)  // even led index
    {
      uc &= 0x1d;   // clear bits 5 - 7, 1 (bit 1 is for tail light)
      uc |= (unsigned char)(ledColor << 5);
      XLport[portIdx].LightData[baseIdx] = uc;
      if (ledStatus == LT_BLINKING)
        uc |= 0xe0;           // set the 3 msbits to blink
      else
        uc &= 0x1f;           // clear 3 msbits for solid ON
      XLport[portIdx].BlinkData[baseIdx] = uc;
    }
    else  // odd led index
    {
      uc &= 0xe2;   // clear bits 2 - 4, 0 (bit 0 is for tail light)
      uc |= (unsigned char)(ledColor << 2);
      XLport[portIdx].LightData[baseIdx] = uc;
      if (ledStatus == LT_BLINKING)
        uc |= 0x1c;           // set bits 2 - 4 to blink
      else
        uc &= 0xe3;           // clear bits 2 - 4 for solid ON
      XLport[portIdx].BlinkData[baseIdx] = uc;

    }
    return(TRUE);     // finished with button light processing
  }
  
  // stick light processing

  d8 = (unsigned char)(ledIdx / 8);
  m8 = (unsigned char)(ledIdx % 8);      // there are 8 possible starting positions for led data 
  baseIdx = (unsigned char)(d8 * 3);     // calculate starting position in array for the RGB led

  switch (m8)
  {
  	case 0:
    	uc = XLport[portIdx].LightData[baseIdx];  // get old data
    	uc &= 0x1f;               // clear 3 msbits
    	uc |= (unsigned char)(ledColor << 5);    // set new light data
    	XLport[portIdx].LightData[baseIdx] = uc;  // update array
    	uc = XLport[portIdx].BlinkData[baseIdx];
    	if (ledStatus == LT_BLINKING)
      		uc |= 0xe0;           // set the 3 msbits to blink
    	else
      		uc &= 0x1f;           // clear 3 msbits for solid ON
    	XLport[portIdx].BlinkData[baseIdx] = uc;
    	break;
	case 1:
		uc = XLport[portIdx].LightData[baseIdx];  // get old data
    	uc &= 0xe3;               // clear bits B4 B3 B2
    	uc |= (unsigned char)(ledColor << 2);    // set new light data
    	XLport[portIdx].LightData[baseIdx] = uc;  // update array
    	uc = XLport[portIdx].BlinkData[baseIdx];
    	if (ledStatus == LT_BLINKING)
      		uc |= 0x1c;           // set bits B4 B3 B2 to blink
    	else
      		uc &= 0xe3;           // clear bits B4 B3 B2 for solid ON
    	XLport[portIdx].BlinkData[baseIdx] = uc;
    	break;
	case 2:		// 2 bytes to modify for this scenerio
		uc = XLport[portIdx].LightData[baseIdx];  // get old data
    	uc &= 0xfc;               // clear bits B1 B0
    	uc |= (unsigned char)(ledColor >> 1);    // set 2 bits of new light data
    	XLport[portIdx].LightData[baseIdx] = uc;  // update array
		uc = XLport[portIdx].LightData[baseIdx+1];  // get old data of next byte
    	uc &= 0x7f;               // clear bit B7
    	uc |= (unsigned char)(ledColor << 7);    // set 1 bit of new light data
    	XLport[portIdx].LightData[baseIdx+1] = uc;  // update array

	   	uc = XLport[portIdx].BlinkData[baseIdx];
    	if (ledStatus == LT_BLINKING)
      		uc |= 0x03;           // set bits B1 B0 to blink
    	else
      		uc &= 0xfc;           // clear bits B1 B0 for solid ON
    	XLport[portIdx].BlinkData[baseIdx] = uc;
		uc = XLport[portIdx].BlinkData[baseIdx+1];
    	if (ledStatus == LT_BLINKING)
      		uc |= 0x80;           // set bit B7 to blink
    	else
      		uc &= 0x7f;           // clear bit B7 for solid ON
    	XLport[portIdx].BlinkData[baseIdx+1] = uc;
    	break;
  	case 3:
    	baseIdx++;			// all operations are in the 2nd byte
		uc = XLport[portIdx].LightData[baseIdx];  // get old data
    	uc &= 0x8f;               // clear B6 B5 B4
    	uc |= (unsigned char)(ledColor << 4);    // set new light data
    	XLport[portIdx].LightData[baseIdx] = uc;  // update array
    	uc = XLport[portIdx].BlinkData[baseIdx];
    	if (ledStatus == LT_BLINKING)
      		uc |= 0x70;           // set B6 B5 B4 to blink
    	else
      		uc &= 0x8f;           // clear B6 B5 B4 for solid ON
    	XLport[portIdx].BlinkData[baseIdx] = uc;
    	break;
	case 4:
    	baseIdx++;			// all operations are in the 2nd byte
		uc = XLport[portIdx].LightData[baseIdx];  // get old data
    	uc &= 0xf1;               // clear B3 B2 B1
    	uc |= (unsigned char)(ledColor << 1);    // set new light data
    	XLport[portIdx].LightData[baseIdx] = uc;  // update array
    	uc = XLport[portIdx].BlinkData[baseIdx];
    	if (ledStatus == LT_BLINKING)
      		uc |= 0x0e;           // set B3 B2 B1 to blink
    	else
      		uc &= 0xf1;           // clear B3 B2 B1 for solid ON
    	XLport[portIdx].BlinkData[baseIdx] = uc;
    	break;
	case 5:		// 2 bytes to modify for this scenerio
		baseIdx++;		// working on 2nd & 3rd bytes
		uc = XLport[portIdx].LightData[baseIdx];  // get old data
    	uc &= 0xfe;               // clear bit B0
    	uc |= (unsigned char)(ledColor >> 2);    // set 1 bit of new light data
    	XLport[portIdx].LightData[baseIdx] = uc;  // update array
		uc = XLport[portIdx].LightData[baseIdx+1];  // get old data of next byte
    	uc &= 0x3f;               // clear bits B7 B6
    	uc |= (unsigned char)(ledColor << 6);    // set 2 bits of new light data
    	XLport[portIdx].LightData[baseIdx+1] = uc;  // update array

	   	uc = XLport[portIdx].BlinkData[baseIdx];
    	if (ledStatus == LT_BLINKING)
      		uc |= 0x01;           // set bit B0 to blink
    	else
      		uc &= 0xfe;           // clear bit B0 for solid ON
    	XLport[portIdx].BlinkData[baseIdx] = uc;
		uc = XLport[portIdx].BlinkData[baseIdx+1];
    	if (ledStatus == LT_BLINKING)
      		uc |= 0xc0;           // set bits B7 B6 to blink
    	else
      		uc &= 0x3f;           // clear bits B7 B6 for solid ON
    	XLport[portIdx].BlinkData[baseIdx+1] = uc;
    	break;
	case 6:
    	baseIdx += 2;			// all operations are in the 3nd byte
		uc = XLport[portIdx].LightData[baseIdx];  // get old data
    	uc &= 0xc7;               // clear B5 B4 B3
    	uc |= (unsigned char)(ledColor << 3);    // set new light data
    	XLport[portIdx].LightData[baseIdx] = uc;  // update array
    	uc = XLport[portIdx].BlinkData[baseIdx];
    	if (ledStatus == LT_BLINKING)
      		uc |= 0x38;           // set B5 B4 B3 to blink
    	else
      		uc &= 0xc7;           // clear B5 B4 B3 for solid ON
    	XLport[portIdx].BlinkData[baseIdx] = uc;
    	break;
	case 7:
    	baseIdx += 2;			// all operations are in the 3nd byte
		uc = XLport[portIdx].LightData[baseIdx];  // get old data
    	uc &= 0xf8;               // clear B2 B1 B0
    	uc |= (unsigned char)(ledColor);    // set new light data
    	XLport[portIdx].LightData[baseIdx] = uc;  // update array
    	uc = XLport[portIdx].BlinkData[baseIdx];
    	if (ledStatus == LT_BLINKING)
      		uc |= 0x07;           // set B2 B1 B0 to blink
    	else
      		uc &= 0xf8;           // clear B2 B1 B0 for solid ON
    	XLport[portIdx].BlinkData[baseIdx] = uc;
    	break;
  }
  
  return(TRUE);   // indicate success

}

// -------------------------------------------------------------------------------------------------------- 
//
// ColorFillPort(unsigned char port, unsigned char ledColor, unsigned char ledStatus)
//   Updates the memory locations for the entire port to the specified color & status 
//   Memory is formatted for output to the SPI port, for quick updates  
//     Inputs:  portIdx range 0 -> (MAX_XL_PORTS - 1)
//              ledColor format: logic 1 turns on the led, b0: blue, b1: green, b2: red
//              ledStatus: LT_ON, LT_OFF, LT_BLINKING (see  trakConstants.h)
//     Returns: nothing
//              
//     SPI port format for stick lights: (pattern repeats every 3 bytes) logic 0 turns on LED
//     B7                   B0 | B7                   B0 | B7                   B0 |
//     R0 G0 B0 R1 G1 B1 R2 G2 | B2 R3 G3 B3 R4 G4 B4 R5 | G5 B5 R6 G6 B6 R7 G7 B7 |
// -------------------------------------------------------------------------------------------------------- 
//     SPI port format for button lights: (1 byte per buttonlight)
//     B7                     B0  |
//     R0 G0 B0 R1 G1 B1 TL0 TL1 |
//--------------------------------------------------------------------------------------------
// R -> Red LED    G -> Green LED    B -> Blue LED     TL -> tail light
//--------------------------------------------------------------------------------------------
void 
ColorFillPort(unsigned char portIdx, unsigned char ledColor, unsigned char ledStatus)
{
  unsigned char lastByte, i;

  if (ledStatus == LT_OFF)								// hardware requires a logic 0 to turn on LED
    ledColor = LT_NOCOLOR;    // status trumps color when it is LT_OFF

  /*  if ((XLConfigPort[portIdx].deviceType[0] == XL_BL_1_BUTTON) ||   // an XL port will contain either all button lights OR all stick lights
      (XLConfigPort[portIdx].deviceType[0] == XL_BL_2_BUTTON))       // check device Type of 1st device on port
  {
    baseIdx = (unsigned char)(ledIdx/2);   // for button lights, each byte contains 2 RGB data and 2 tail light data
    ledNumEvenOdd = (unsigned char)(ledIdx % 2);    // even: 0 (b7 - b5)   odd: 1 (b4 - b2)
    uc = XLport[portIdx].LightData[baseIdx];  // get old data
    if (ledNumEvenOdd == 0)  // even led index
    {
      uc &= 0x1d;   // clear bits 5 - 7, 1 (bit 1 is for tail light)
      uc |= (unsigned char)(ledColor << 5);
      XLport[portIdx].LightData[baseIdx] = uc;
      if (ledStatus == LT_BLINKING)
        uc |= 0xe0;           // set the 3 msbits to blink
      else
        uc &= 0x1f;           // clear 3 msbits for solid ON
      XLport[portIdx].BlinkData[baseIdx] = uc;
    }
    else  // odd led index
    {
      uc &= 0xe2;   // clear bits 2 - 4, 0 (bit 0 is for tail light)
      uc |= (unsigned char)(ledColor << 2);
      XLport[portIdx].LightData[baseIdx] = uc;
      if (ledStatus == LT_BLINKING)
        uc |= 0x1c;           // set bits 2 - 4 to blink
      else
        uc &= 0xe3;           // clear bits 2 - 4 for solid ON
      XLport[portIdx].BlinkData[baseIdx] = uc;

    }
    return(TRUE);     // finished with button light processing
  }
*/  
  // stick light processing

  lastByte = ((MAX_XL_STICKS_PER_PORT * 4) + (MAX_XL_STICKS_PER_PORT / 2) + 1);
  i= 0; // start at first byte of ram image of port (portIdx)
  

  switch (ledColor)
  {
  	case LT_NOCOLOR:
    	while (1) {
            XLport[portIdx].LightData[i] = 0xff;
            XLport[portIdx].BlinkData[i] = 0;       // can't blink when lights are off
            if (++i >= lastByte) 
                break;
        }
        break;
    
    case LT_BLUE:       // turn on all blue LEDs (logic 0 = ON)
		while (1) {
            XLport[portIdx].LightData[i] = 0xdb;
            if (ledStatus == LT_BLINKING) 
                XLport[portIdx].BlinkData[i] = 0x24; // logic 1 blinks
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte) 
                break;
            
            XLport[portIdx].LightData[i] = 0x6d;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0x92;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte)
                break;

            XLport[portIdx].LightData[i] = 0xb6;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0x49;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte)
                break;
        }
    	break;

	case LT_LIME:		// turn on all green LEDs (logic 0 = ON)
        while (1) {
            XLport[portIdx].LightData[i] = 0xb6;
            if (ledStatus == LT_BLINKING) 
                XLport[portIdx].BlinkData[i] = 0x49; // logic 1 blinks
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte) 
                break;
            
            XLport[portIdx].LightData[i] = 0xdb;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0x24;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte)
                break;

            XLport[portIdx].LightData[i] = 0x6d;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0x92;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte)
                break;
        }
        break;

  	case LT_CYAN:       // turn on all blue and green LEDs (logic 0 = ON)
    	while (1) {
            XLport[portIdx].LightData[i] = 0x92;
            if (ledStatus == LT_BLINKING) 
                XLport[portIdx].BlinkData[i] = 0x6d; // logic 1 blinks
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte) 
                break;
            
            XLport[portIdx].LightData[i] = 0x49;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0xb6;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte)
                break;

            XLport[portIdx].LightData[i] = 0x24;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0xdb;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte)
                break;
        }
        break;

	case LT_RED:   // turn on all red LEDs (logic 0 = ON)
    	while (1) {
            XLport[portIdx].LightData[i] = 0x6d;
            if (ledStatus == LT_BLINKING) 
                XLport[portIdx].BlinkData[i] = 0x92; // logic 1 blinks
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte) 
                break;
            
            XLport[portIdx].LightData[i] = 0xb6;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0x49;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte)
                break;

            XLport[portIdx].LightData[i] = 0xdb;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0x24;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte)
                break;
        }
    	break;

	case LT_MAGENTA:		// turn on all red and blue LEDs (logic 0 = ON)
		while (1) {
            XLport[portIdx].LightData[i] = 0x49;
            if (ledStatus == LT_BLINKING) 
                XLport[portIdx].BlinkData[i] = 0xb6; // logic 1 blinks
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte) 
                break;
            
            XLport[portIdx].LightData[i] = 0x24;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0xdb;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte)
                break;

            XLport[portIdx].LightData[i] = 0x92;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0x6d;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte)
                break;
        }
    	break;

	case LT_YELLOW:      // turn on all red and green LEDs (logic 0 = ON)
    	while (1) {
            XLport[portIdx].LightData[i] = 0x24;
            if (ledStatus == LT_BLINKING) 
                XLport[portIdx].BlinkData[i] = 0xdb; // logic 1 blinks
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte) 
                break;
            
            XLport[portIdx].LightData[i] = 0x92;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0x6d;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte)
                break;

            XLport[portIdx].LightData[i] = 0x49;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0xb6;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte)
                break;
        }
    	break;

	case LT_WHITE:          // turn on all LEDs (logic 0 = ON)
    	while (1) {
            XLport[portIdx].LightData[i] = 0x00;
            if (ledStatus == LT_BLINKING)
                XLport[portIdx].BlinkData[i] = 0xff;
            else
                XLport[portIdx].BlinkData[i] = 0;
            if (++i >= lastByte) 
                break;
        }
    	break;
  }  // end of switch
}

/// **************************************************************** 
//
// SendPortLEDdata(unsigned char portIdx,unsigned char blinkControl)
// Writes current LED data to the specified XL port via the SPI port
//    inputs: portIdx -> port to update, range 0 -> MAX_XL_PORTS - 1
//            Stick_Count -> # of sticks on (Shelf_Number)
//          : blinkControl -  if TRUE, blank LEDs with the flash attribute
//
// **************************************************************** 

void SendPortLEDdata(unsigned char portIdx, unsigned char blinkControl)
{
 
    unsigned char SPIoutputByte, deviceCount;

    //**************** test: measure port update time *****************************************
    //PTDDD = 0xff;                   // config PORT D bit 7..0 to outputs
    //PORT_PTDD = 0x10;          // port D bit 4 = 1 to indicate start of routine

	//deviceCount = XLConfigPort[portIdx].deviceCount;

    //if (deviceCount == 0)
	  //return;
    
    if ((XLConfigPort[portIdx].deviceType[0] == XL_BL_1_BUTTON) ||   // an XL port will contain either all button lights OR all stick lights
      (XLConfigPort[portIdx].deviceType[0] == XL_BL_2_BUTTON))       // check device Type of 1st device on port
    {
      SPI_1_outputPtr = MAX_XL_BUTTONLIGHTS_PER_PORT;   // for button lights, output 1 byte per device
    }
    else    // for stick lights (may change pointer if all sticks are XL9 or combination of XL12, XL9)
    {
      SPI_1_outputPtr = (MAX_XL_STICKS_PER_PORT*4) + (MAX_XL_STICKS_PER_PORT/2) + 1; // point to last byte of array
    }
    
    // PTDD = 0;    // ****** test

    //XLportToUpdate = portIdx;
    //XLportBlinkControl = blinkControl;

    ReadyPortForSPIWrite(portIdx);     // Enable XL port for SPI write
    
    uDelay(10);    // let port buffers switch before sending data

    //SPI_1_EnableTransmitInterrupt();
/*
    while (SPI0CNTL1 & SPTIE)
      ;     // test - stay here as long as interrupt enabled
*/    
    
    // initially, SPIoutputPtr points to desired initial location plus 1
    // SPIoutputPtr decrements to the beginning of the buffer
    while (SPI_1_outputPtr != 0)
    {
      SPI_1_outputPtr--;
      if (blinkControl == TRUE)
        SPIoutputByte = (unsigned char)(XLport[portIdx].LightData[SPI_1_outputPtr] | XLport[portIdx].BlinkData[SPI_1_outputPtr]);  // turn off lights with blink attribute set
      else
        SPIoutputByte = XLport[portIdx].LightData[SPI_1_outputPtr];
      
      pollSPI_1_Write(SPIoutputByte);    // send LED data via the SPI port
    }
    
    pollSPI_1_TransmitComplete();     // wait till last byte completely shifted out SPI port

    DisableSPI_ReadWrite(); // disable all SPI sub ports    


} // end of SendPortLEDdata()

 

// **************************************************************** 
//
// BlinkPortLEDS(unsigned char portIdx, unsigned char blinkControl)
// Writes current LED data to the XL port IF there are any LEDs with
//    the flash attribute 
//    inputs: portIdx - XLport to update, range = 0 to MAX_XL_PORTS-1
//            blinkControl -  if TRUE, blank LEDs with the flash attribute
//                               FALSE, turn ON LEDs with the flash attribute
//            
// **************************************************************** 
void 
BlinkPortLEDS(unsigned char portIdx, unsigned char blinkControl)
{
  unsigned char deviceCount, i, j, blinkDataEnd;;

  deviceCount = XLConfigPort[portIdx].deviceCount;
  if (deviceCount == 0)
    return;
  
  if ((XLConfigPort[portIdx].deviceType[0] == XL_BL_1_BUTTON) ||
     (XLConfigPort[portIdx].deviceType[0] == XL_BL_2_BUTTON))
  {
    blinkDataEnd = MAX_XL_BUTTONLIGHTS_PER_PORT;
  }
  else
  {
    blinkDataEnd = (MAX_XL_STICKS_PER_PORT*4) + (MAX_XL_STICKS_PER_PORT/2) + 1;
  }
  j = 0;
  for (i = 0; i < blinkDataEnd; i++)
    j |= XLport[portIdx].BlinkData[i];
	
  if (j != 0)
    SendPortLEDdata(portIdx, blinkControl);

} // FlashPortLEDS


 
void
ClearPortLEDs(unsigned char portIdx)       
{                 
  int i;

  
  for (i = 0; i < ((MAX_XL_STICKS_PER_PORT*4) + (MAX_XL_STICKS_PER_PORT/2) + 1); i++)
  {
    XLport[portIdx].LightData[i] = 0xff;
    XLport[portIdx].BlinkData[i] = 0x00;
  }		
  SendPortLEDdata(portIdx, FALSE);    // update physical LEDs


} // End ClearPortLEDs

void
ClearAllPorts(void)   // clears stick data space to all LEDs off, then updates the physical LEDs,
{                     // also inits the button variables for each stick
  unsigned char iport;

  for (iport = 0; iport < MAX_XL_PORTS; iport++)
	ClearPortLEDs(iport);

}

// blinky lights for when master CPU is being reflashed
void
Download_Display(void)
{
  unsigned char port;
  static unsigned char color = LT_BLUE;
  static unsigned char toggle = FALSE;

  if (toggle == FALSE)
    toggle = TRUE;
  else
    toggle = FALSE;
    
  for (port = 0; port < MAX_XL_PORTS; port++)
  {
    if (XLConfigPort[port].deviceCount > 0)
    {
      if (toggle)
      {
        UpdateLEDMemory(port,0,color,LT_OFF);
        UpdateLEDMemory(port,1,color,LT_SOLID);
      }
      else
      {
        UpdateLEDMemory(port,0,color,LT_SOLID);
        UpdateLEDMemory(port,1,color,LT_OFF);
      }
      SendPortLEDdata(port, FALSE);
    }
  }
  if (++color > LT_WHITE)
    color = LT_BLUE;

}

