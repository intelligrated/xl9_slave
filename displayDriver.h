/******************************
* displayDriver.h             *
* Helper functions to control *
* the XL display              *
*******************************/

#ifndef DISPLAYDRIVER_HEADER
#define DISPLAYDRIVER_HEADER


unsigned char Auto_Config_XL_Ports(void);
void Read_XL_Device_Buttons(unsigned char iport);
unsigned char UpdateLEDMemory(unsigned char port, unsigned char ledIdx, unsigned char ledColor, unsigned char status);
void ColorFillPort(unsigned char portIdx, unsigned char ledColor, unsigned char ledStatus);
void SendPortLEDdata(unsigned char Shelf_Number, unsigned char blinkControl);
void BlinkPortLEDS(unsigned char iport, unsigned char flashControl);
void ClearPortLEDs(unsigned char port);
void ClearAllPorts(void);
void Download_Display(void);

#endif

