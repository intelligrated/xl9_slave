;	C STARTUP FOR MC68HC08/HCS08 with WDOG disable (PT32)
;	WITH AUTOMATIC DATA/CODE INITIALISATION
;	Copyright (c) 2000 by COSMIC Software
;
WDOG_CNTH:   equ $3032
WDOG_CNTL:   equ $3033
WDOG_TOVALH: equ $3034
WDOG_TOVALL: equ $3035
WDOG_CS1:    equ $3030
WDOG_CS2:    equ $3031

	xref	_main, __sbss, __memory, __idesc__, __stack
	xref	_wdog_disable
	xdef	_exit, __stext
;
__stext:
				; you have 128 bus cycles after a reset to
				; reconfigure the watchdog (without unlocking it)
	lda	#$20
	sta	WDOG_CS1	; disable watchdog timer, enable updates
	
	lda	#$01		; wdt clock = 1khz internal osc
	sta	WDOG_CS2	
	
	lda	#$00		; note: you have to write to all 4 registers
	sta	WDOG_TOVALH	; or none of the changes will take effect
	
	lda	#$ff
	sta	WDOG_TOVALL	; set timeout value to 255
	
	ldhx	#__stack	; initialize stack pointer
	txs

;	lda	#$0C5
;        sta	WDOG_CNTH
;	lda	#$20
;	sta	WDOG_CNTL
;	lda	#$0d9
;	sta	WDOG_CNTH
;	lda	#$28
;	sta	WDOG_CNTL
;	lda	#$00		; 256 mS timeout
;	sta	WDOG_TOVALH
;	lda	#$0ff
;	sta	WDOG_TOVALL
;	lda	#$01
;	sta	WDOG_CS2	; wdog clk is 1 khz internal osc
;	lda	#$00
;	sta	WDOG_CS1	; disable watchdog
			
	ldhx	#__idesc__	; descriptor address
cbcl:
	lda	1,x		; save start
	psha			; address of
	lda	0,x		; prom data
	psha
ibcl:
	lda	2,x		; test flag byte
	beq	zbss		; no more segment
	bit	#$60		; code segment
	bne	dseg		; no, copy it
	ais	#2		; remove previous start address
	aix	#5		; next descriptor
	bra	cbcl		; and restart
dseg:
	pshx			; save
	pshh			; pointer
ifdef HCS08
	lda	6,x		; stack end address
	psha
	lda	5,x
	psha
	ldhx	3,x		; destination address in HX
else
	lda	6,x		; compute length
	sub	1,x		; of segment
	psha			; save count MSB
	lda	5,x		; compute LSB
	sbc	0,x
	tst	1,sp		; if LSB nul,
	beq	ok		; keep it
	inca			; else increment MSB
ok:
	psha			; save count LSB
	lda	3,x		; destination address
	psha			; prepared in HX
	ldx	4,x
	pulh
endif
dbcl:
	pshx			; save destination pointer
	pshh
ifdef HCS08
	ldhx	7,sp		; load source pointer
	lda	0,x		; load byte
	aix	#1		; increment
	sthx	7,sp		; and store pointer
else
	ldx	7,sp		; load source pointer
	pshx
	pulh
	ldx	8,sp
	inc	8,sp		; increment pointer
	bne	oks
	inc	7,sp
oks:
	lda	0,x		; load byte
endif
	pulh			; get destination
	pulx			; pointer
	sta	0,x		; store byte
	aix	#1		; next byte
ifdef HCS08
	cphx	1,sp		; reach end ?
	bne	dbcl		; continue
else
	dbnz	2,sp,dbcl	; count LSB
	dbnz	1,sp,dbcl	; count MSB
endif
	ais	#2		; cleanup stack
	pulh			; reload pointer
	pulx
	aix	#5		; next descriptor
	bra	ibcl		; and loop
zbss:
	ais	#2		; remove pointer
	ldhx	#__sbss		; start of bss
	bra	loop		; start loop
zbcl:
	clr	0,x		; clear byte
	aix	#1		; next byte
loop:
	cphx	#__memory	; up to the end
	bne	zbcl		; and loop
prog:
	jsr	_main		; execute main
_exit:
	bra	_exit		; and stay here
;
	end

